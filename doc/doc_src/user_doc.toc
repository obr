\select@language {english}
\contentsline {chapter}{\numberline {1}User Documentation}{3}
\contentsline {section}{\numberline {1.1}Introduction}{3}
\contentsline {section}{\numberline {1.2}Installation}{3}
\contentsline {section}{\numberline {1.3}Usage}{4}
\contentsline {subsection}{\numberline {1.3.1}Invocation}{4}
\contentsline {subsection}{\numberline {1.3.2}Navigation}{4}
\contentsline {paragraph}{Image view}{4}
\contentsline {paragraph}{Thumbnail view}{4}
\contentsline {subsection}{\numberline {1.3.3}Photo Album}{4}
\contentsline {section}{\numberline {1.4}Licensing information}{5}
