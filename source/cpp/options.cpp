#include "options.h"

using std::cout;
using std::endl;

extern boost::mutex global_iomutex;

Const::Keys::KeyReturn Options::translate_from_glut_normal_keycode(unsigned char key){
	using namespace Const::Keys;

	boost::mutex::scoped_lock options_lock(mutex);

	Const::Keys::KeyReturn ret;
	ret.key = key;

	if(key == 'h' || key == 'H') ret.keycode = Left;
	if(key == 'j' || key == 'J') ret.keycode = Down;
	if(key == 'k' || key == 'K') ret.keycode = Up;
	if(key == 'l' || key == 'L') ret.keycode = Right;
	if(key == 's' || key == 'S') ret.keycode = Slideshow;
	if(key == 'f' || key == 'F') ret.keycode = Fullscreen;
	if(key == 't' || key == 'T') ret.keycode = Thumbnails;
	if(key == 'm' || key == 'M') ret.keycode = Mark;
	if(key == 'i' || key == 'I') ret.keycode = Info;
	if(key == 27 || key == 'q' || key == 'Q') ret.keycode = Escape;
	if(key == ' ') ret.keycode = Space;
	if(key == 8) ret.keycode = Backspace;
	if(key == 13) ret.keycode = Enter;
	if(key == 127) ret.keycode = Delete;
	
	return ret;
}

Const::Keys::KeyReturn Options::translate_from_glut_special_keycode(int key){
	using namespace Const::Keys;

	boost::mutex::scoped_lock options_lock(mutex);

	Const::Keys::KeyReturn ret;

	if(key == GLUT_KEY_LEFT) ret.keycode = Left;	
	if(key == GLUT_KEY_RIGHT) ret.keycode = Right;	
	if(key == GLUT_KEY_UP) ret.keycode = Up;	
	if(key == GLUT_KEY_DOWN) ret.keycode = Down;	
	if(key == GLUT_KEY_HOME) ret.keycode = Home;	
	if(key == GLUT_KEY_END) ret.keycode = End;	

	return ret;
}

void Options::print(){
	boost::mutex::scoped_lock io_lock(global_iomutex);
	boost::mutex::scoped_lock options_lock(mutex);

	cout << "BeginOptions" << endl;

	if(transition_effect == Const::TransitionSkip)
		cout << "TransitionSkip" << endl;
	else
		cout << "TransitionSmooth" << endl;

	if(	max_texture_size == 512 )
		cout << "MaxTextureSize512" << endl;
	else
	if(	max_texture_size == 2048 )
		cout << "MaxTextureSize2048" << endl;
	else
	if(	max_texture_size == 4096 )
		cout << "MaxTextureSize4096" << endl;
	else
		cout << "MaxTextureSize1024" << endl;
	cout << "Verbose" << verbose << endl;

	cout << "SlideshowDelay" << endl << slideshow_delay << endl;
	cout << "FontSize" << endl << font_size << endl;
	cout << "BorderSize" << endl << border_size << endl;
	cout << "MinThumbnailSize" << endl << min_thumbnail_size << endl;
	cout << "Font" << endl << font << endl;
	cout << "RotationDelay" << endl << rotation_delay << endl;
	cout << "EndOptions" << endl;
}
