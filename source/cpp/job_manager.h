#ifndef _MEMORY_MAN
#define _MEMORY_MAN

#include <boost/thread/mutex.hpp>
#include <iostream>
//#include <string>
//#include <algorithm>
#include <vector>
#include <stdexcept>
#include <stdlib.h>

#include "threading.h"
#include "image.h"
#include "options.h"

/*		Class inside Main object, that handles		*/
/*		ordering of images for Faces and Main		*/
class JobManager{
public:
	JobManager( TaskMutexQueue & tmq, TaskMutexQueue & res_tmq ): thread_job_queue(tmq), result_queue(res_tmq), max_pr(0) {};
	bool order_image(ImageData * image, int priority);
	bool order_image(ImageData * image){
		return order_image(image, max_pr + 1);
	}
	long int max_priority(){
		return max_pr;
	}
	/*		A function that puts a special			*/
	/*		Task, saying that the thread should		*/
	/*		die.			*/
	void quit(){
		Task * task = new Task(Signal::Quit);
		thread_job_queue.push(task, ++max_pr);
	}
	/*		A predicate to test whether a queue		*/
	/*		is empty		*/
	bool no_job(){
		return thread_job_queue.empty();
	}

private:
	TaskMutexQueue & thread_job_queue;
	TaskMutexQueue & result_queue;
	/*		Used to store max priority put		*/
	/*		so far in the queue.				*/
	long int max_pr;
};

#endif
