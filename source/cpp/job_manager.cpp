#include "job_manager.h"
extern Options * global_options;
extern boost::mutex global_iomutex;
using namespace std;
bool JobManager::order_image(ImageData * image, int priority){
	if(image){
		if(priority > max_pr)
			max_pr= priority;
		if(	image->test_empty()){

		
		if(global_options->get_verbose()) {
		boost::mutex::scoped_lock io_lock(global_iomutex);
		cerr << "Order Imlib: "<< image->name << endl;
		}
			Task * task = new TaskImageLoadImlib(image);
			thread_job_queue.push(task, priority);
			return 1;
		}
		if(	image->test_imlib_ready()){
			Task * task = new TaskImageContainer(image);
			result_queue.push(task, priority);
			return 1;
		}
	}
	return 0;
}
