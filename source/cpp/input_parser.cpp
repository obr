#include "input_parser.h"

// get input tokens
Tokenizer * Parser::get_tokens(){
	boost::char_separator<char> sep(" ");
	string str;
	getline(in,str); 
	return new Tokenizer(str, sep);
}
// read an unsigned number from the in stream
unsigned Parser::read_num(){
	unsigned x=0;
	char c=in.peek();

	while(c >= '0' && c <= '9'){
		x = x*10 + c - '0';
		in.get();
		c=in.peek();
	}

	return x;
}
// Parse the input, output goes to `options' and `slides'
void Parser::operator()(Options * options, std::vector<Slide *> * slides){
    while(!in.eof()){
		// get tokens separated by spaces	
        Tokenizer * tokens = get_tokens();
		// if none get next line
        if( tokens->begin() == tokens->end() )
            continue;
		// if slide block
        if( *(tokens->begin()) == "BeginSlide" ){ 
            delete tokens;
            Slide * slide = new Slide;
			// parse the slide until it ends (we break from the loop)
            for(;;){
                tokens = get_tokens();
                if(in.eof()){
                    delete tokens;
                    delete slide;
                    break;
                }
                if(tokens->begin() == tokens->end()){
                    delete tokens;
                    continue;
                }
				// if end of the slide, push the current slide to the `slides' vector
                if(*(tokens->begin()) == "EndSlide" ){
                    if( slide->image == 0 ){
                        std::cerr << "Slide without image!" << std::endl;
                        delete slide;
                    }else
                       slides->push_back(slide); 
                    delete tokens;
                    break;
                }
				// different inputs
                if(*(tokens->begin()) == "Rotation0" ){ slide->rot = 0; delete tokens; continue; }
                if(*(tokens->begin()) == "Rotation90" ){ slide->rot = 90; delete tokens; continue; }
                if(*(tokens->begin()) == "Rotation180" ){ slide->rot = 180; delete tokens; continue; }
                if(*(tokens->begin()) == "Rotation270" ){ slide->rot = 270; delete tokens; continue; }
                if(*(tokens->begin()) == "Marked1" ){ slide->marked = 1; delete tokens; continue; }
                if(*(tokens->begin()) == "Marked0" ){ slide->marked = 0; delete tokens; continue; }
                if( *(tokens->begin()) == "Image" || *(tokens->begin()) == "Histogram"
                        || *(tokens->begin()) == "Thumbnail" ||  *(tokens->begin()) == "Text"   ){
                    string old=*(tokens->begin());
                    string str;
                    getline(in, str);
                    if(in.eof()){
                        std::cerr << "No "<< old <<" specified." << std::endl;
                        delete tokens;
                        delete slide;
                        break;
                    }
                    if( old == "Image")
                       slide->image = new ImageData(str);
                    else if( old == "Thumbnail")
                       slide->thumbnail = new ImageData(str);
                    else if( old == "Histogram")
                       slide->histogram = new ImageData(str);
                    else 
                       slide->text = new string(str);
                        
                }
                delete tokens;
            }
            continue;
        }
		// Options block
        if( *tokens->begin() == "BeginOptions" ){ 
            delete tokens;
			// parse options until the block is over
            for(;;){
                tokens = get_tokens();
                if(in.eof()){
                    delete tokens;
                    break;
                }
                if(tokens->begin() == tokens->end()){
                    delete tokens;
                    continue;
                }
                if(*(tokens->begin()) == "EndOptions" ){ delete tokens; break; }
                if(*(tokens->begin()) == "TransitionSmooth" ){
                    options->set_transition_effect(Const::TransitionSmooth);
                    delete tokens; continue;
                }
                if(*(tokens->begin()) == "TransitionSkip" ){
                    options->set_transition_effect(Const::TransitionSkip);
                    delete tokens; continue;
                }
                if(*(tokens->begin()) == "MaxTextureSize512" ){ options->set_max_texture_size(512); delete tokens; continue; }
                if(*(tokens->begin()) == "MaxTextureSize1024" ){ options->set_max_texture_size(1024); delete tokens; continue; }
                if(*(tokens->begin()) == "MaxTextureSize2048" ){ options->set_max_texture_size(2048); delete tokens; continue; }
                if(*(tokens->begin()) == "MaxTextureSize4096" ){ options->set_max_texture_size(4096); delete tokens; continue; }

                if(*(tokens->begin()) == "Verbose0" ){ options->set_verbose(0); delete tokens; continue; }
                if(*(tokens->begin()) == "Verbose1" ){ options->set_verbose(1); delete tokens; continue; }

                if(*(tokens->begin()) == "SlideshowDelay" ){
					unsigned num = read_num(); if(num) options->set_slideshow_delay(num); delete tokens; continue; }
                if(*(tokens->begin()) == "FontSize" ){
					unsigned num = read_num(); if(num) options->set_font_size(num); delete tokens; continue; }
                if(*(tokens->begin()) == "BorderSize" ){
					unsigned num = read_num(); if(num) options->set_border_size(num); delete tokens; continue; }
                if(*(tokens->begin()) == "MinThumbnailSize" ){
					unsigned num = read_num(); if(num) options->set_min_thumbnail_size(num); delete tokens; continue; }
                if(*(tokens->begin()) == "RotationDelay" ){
					unsigned num = read_num(); if(num) options->set_rotation_delay(num); delete tokens; continue; }

                if( *(tokens->begin()) == "Font"  ) {
                    string str;
                    getline(in, str);
                    if(in.eof()){
                        std::cerr << "No font specified." << std::endl;
                        delete tokens; break;
                    }
				   options->set_font(str);
                }
                delete tokens;
            }
            continue;
        }
        delete tokens;
    }
}
