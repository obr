#ifndef _THUMBNAIL_FACE
#define _THUMBNAIL_FACE

#include <GL/gl.h>
#include <FTGL/ftgl.h>
#include <stdexcept>
#include <vector>

#include "image.h"
#include "options.h"
#include "job_manager.h"
#include "face.h"
/*		Class drawing thumbnails		*/
class ThumbnailFace: public Face{
public:
	ThumbnailFace(std::vector<Slide *> & slds, JobManager & jbm);
	~ThumbnailFace();

	/*		Face API						*/
	virtual void draw(unsigned);
	virtual void reshape(int w, int h);

	/*		Movement in the thumbnail face	*/
	void move_up();
	void move_down();
	void move_left();
	void move_right();
	void move_end();  // move to the last image
	void move_home(); // move to the first image
	void move_prev();
	void move_next();
	/*		Returns a current slide			*/
	unsigned get_current();
	/*		Order Image data if necessary 	*/
	void order_necessary_images();
private:
	/*		Movement		*/
	void move_up(bool order);
	void move_down(bool order);

	/*		Since we draw a rectangular 	*/
	/*		thumbnail shape, we need to		*/
	/*		compute the new beginning of 	*/
	/*		the rect. See one_screen below	*/
	void update_screen_beg();
	void compute_new_screen_beg();

	/*		Function drawing a selection	*/
	/*		polygon around a rect defined	*/
	/*		by pos.							*/
	void draw_current_selection(const Position * pos);

	/*		Image ordering		*/
	bool load_checked(int index, int priority);
	bool load_checked(int index);
	void order_images(int a, int b);
	
	/*		References to main data objects		*/
	std::vector<Slide *> & slides;
	/*		Each thumbnail texture has		*/
	/*		a position.						*/
	std::vector<Position *> positions;
	/*		Ref. to object we use to 		*/
	/*		order images.					*/
	JobManager & job_manager;


	const Position * null_pos;
	
	/*		Indexes in the Slides		*/
	unsigned current_slide;
	unsigned current_screen_beg;
	/*		Thumbnail face draws a rectangular		*/
	/*		portion of Slides. This rectangle		*/
	/*		has a  slides_num_w * slides_num_h		*/
	/*		elements and this happens to be			*/
	/*		stored in one_screen var.				*/
	unsigned slides_num_w;
	unsigned slides_num_h;
	unsigned one_screen;
	
	/*		Window size		*/
	int screen_w, screen_h;
	/*		Vars used to draw the thumbnails		*/
	/*		The first thumbnail has center in		*/
	/*		[startx, starty], each under has 		*/
	/*		coordinates [startx, starty+stepy]		*/
	/*		and so on.			*/
	GLfloat startx, starty, stepx, stepy;
	/*		A gap between the window border			*/
	/*		and thumbs. and between the thumbs 		*/
	/*		themselves.		*/
	GLfloat border_in_gl_units;	
};


#endif
