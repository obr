#ifndef _IMAGEH
#define _IMAGEH

#include <boost/thread/mutex.hpp>
#include <GL/gl.h>
#include <Imlib2.h>
#include <iostream>
#include <string>
#include <stdexcept>

#include "threading.h"
#include "options.h"

using std::string;

/*		structure holding imlib data			*/
/*		= raw image data loaded from disk		*/
struct ImlibData {
	/*		Data stored in ABGR=4444 format		*/
	/*		line by line.						*/
	/*		size = texture_size*texture_size	*/
	/*		(GL needs square textures)			*/
	/*		the image itself is sized w*h		*/
	/*		See									*/
	/*		ImlibData(const string & name) source	*/
	DATA32 * data;
	int texture_size;
	int w,h;
	/*		The constructor 		*/
	ImlibData(const string & name);
	virtual ~ImlibData(){
		delete [] data;
	}
};

/*		 Structure holding openGL texture		*/
/*		 and its properties						*/
struct TextureData {
	/*		The number of texture as 			*/
	/*		assigned by GL						*/
	/*		(GL needs square textures)			*/
	GLuint texture;
	GLfloat xt1,yt1,xt2,yt2;        // left bottom corner and top right corner
	/*		The same as in ImlibData			*/
	int w,h;
    unsigned int texture_size;

    TextureData (const ImlibData & data);
    ~TextureData(){ 
		if(texture !=0)
			glDeleteTextures(1, &texture);
	}
};

/*		Mutexed (See StateObject in threading.h) 		*/
/*		object used to hold image data					*/
class ImageData: public StateObject{
public:
	/*		Gl, resp. raw data pointers		*/
    TextureData * data;
	ImlibData * imlib_data;
	/*		and loading			*/
	void load_imlib_data(void);
	void load_data(void);
	/*		and deleting		*/
	void delete_imlib_data(void);
	void delete_data(void);
	void delete_all(void);
	/*		Image name (filename) 	*/
    string name;
	/*		Age of image			*/
	/*		Used to discard images that		*/
	/*		have not long been in use.		*/
	unsigned age;

    ImageData(const string &def_name):data(0),imlib_data(0),name(def_name), age(0){}
    ImageData(const char * def_name):data(0),imlib_data(0),name(def_name), age(0){}

    virtual ~ImageData(){
        delete_data();
		delete_imlib_data();
    }
};
/*		 Task that holds pointer to loaded		*/
/*		ImageData, used to givew results from	*/
/*		the thread.		*/
class TaskImageContainer:public Task{
	public:
		TaskImageContainer(ImageData * def_data, int _sig): Task(_sig), data(def_data) {}
		TaskImageContainer(ImageData * def_data): data(def_data){}
		ImageData * data;
};
/*		 Task that loads imlib data			*/
class TaskImageLoadImlib:public Task{
	public:
		TaskImageLoadImlib(ImageData * def_data):data(def_data){}
		Task * do_it();
	private:
		ImageData * data;
};
/*		The root structure for holding		*/
/*		information and data about 			*/
/*		different image objects.			*/
struct Slide{
	/*		Print the slide in the same 		*/
	/*		way that is read by the input		*/
	/*		Parser		*/
	void print();
	int rot; 			// image rotation
    bool marked; 		// marked flag
	string * text;
	/*		Images		*/
    ImageData *image, *thumbnail, *histogram;

    Slide(): rot(0),marked(0),text(0),image(0),thumbnail(0),histogram(0){}
    ~Slide() {
		delete text;
        delete image;
        delete thumbnail;
        delete histogram;
    }
};
#endif
