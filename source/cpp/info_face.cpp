#include "info_face.h"

extern Options * global_options;

InfoFace::InfoFace(FTFont* ft):slideshow(0), ft_font(ft), info_pos(0), slide_pos(0){
}

InfoFace::~InfoFace(){
	delete info_pos;
	delete slide_pos;
}
// update current text shown
void InfoFace::update(unsigned current_slide, unsigned slides_total, bool _slideshow){
	slideshow = _slideshow;
	std::ostringstream str;

	str <<" "<< current_slide + 1 << "/" << slides_total;
	info_text =	str.str();
}

// compute text coordinates so that it fits in the window
void InfoFace::reshape(int w, int h){
	int def_ft_size = global_options->get_font_size();
	{
		if(!info_pos) info_pos = new FTPosition;
		if(def_ft_size >= h/8)
			info_pos->face_size=h/8;
		else
			info_pos->face_size=def_ft_size;
		ft_font->FaceSize(info_pos->face_size);
		float width_of_the_text = (GLfloat) 2*ft_font->Advance(info_text.c_str())/w +(float) 8/w;
		if(width_of_the_text> 1.0){
			info_pos->face_size = (int) (info_pos->face_size /width_of_the_text);
			info_pos->x= 0;
		}
		else
			info_pos->x= 1-width_of_the_text;
		info_pos->y= (GLfloat) h / w - (GLfloat) 2*info_pos->face_size / w ;
		info_pos->z=1.01;
		info_pos->xs=info_pos->x + (GLfloat) info_pos->face_size /(8 * w);
		info_pos->ys=info_pos->y + (GLfloat) info_pos->face_size /(10 * w);
		info_pos->zs=1.009;

		if(!slide_pos) slide_pos = new FTPosition;
		slide_pos->y = info_pos->y;
		slide_pos->face_size = info_pos->face_size;
		ft_font->FaceSize(slide_pos->face_size);
		width_of_the_text = (GLfloat) 2*ft_font->Advance(Const::DefaultSlideshowText.c_str())/w +(float) 8/w;
		slide_pos->x=info_pos->x - width_of_the_text;
		slide_pos->xs=slide_pos->x + (GLfloat) slide_pos->face_size /(8 * w);
		slide_pos->ys=slide_pos->y + (GLfloat) slide_pos->face_size /(10 * w);
		slide_pos->zs=1.009;
		slide_pos->z=1.01;
	}
}
// draw the text
void InfoFace::draw(unsigned info){
	// if verbose
	if(info & 1){
		if(info_pos)
			draw_text(ft_font, info_pos, info_text.c_str() , 0.8, 0.1, 0.1);
		if(slide_pos && slideshow)
			draw_text(ft_font, slide_pos, Const::DefaultSlideshowText.c_str() , 0.8, 0.1, 0.1);
	}
}
