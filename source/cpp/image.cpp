#include "image.h"

using std::cout;
using std::cerr;
using std::endl;

extern Options * global_options;
extern boost::mutex global_iomutex;

void Slide::print(){
	boost::mutex::scoped_lock io_lock(global_iomutex);
    cout << "BeginSlide"<< endl;
	cout << "Rotation" << rot << endl;
	cout << "Marked" << marked << endl;
    if(image) cout << "Image" << endl << image->name << endl;
    if(thumbnail) cout << "Thumbnail" << endl << thumbnail->name << endl;
    if(histogram) cout << "Histogram" << endl << histogram->name << endl;
    if(text) cout << "Text" << endl << *text << endl;
    cout << "EndSlide"<< endl;
}

// Task to job_queue that loads imlib_data from the disk
Task * TaskImageLoadImlib::do_it(){
	if(data && data->test_empty()){
		int signal=Signal::OK;
		try{
			data->load_imlib_data();
		}
		catch(std::bad_alloc){
			boost::mutex::scoped_lock data_lock(data->state_mutex);
			data->state = State::Empty;
			signal = Signal::SigBadAlloc;
		}
		catch(std::exception){
			boost::mutex::scoped_lock io_lock(global_iomutex);
			boost::mutex::scoped_lock data_lock(data->state_mutex);
			std::cerr << "Could not load image: "<< data->name << std::endl;
			signal = Signal::Error;
		}

		Task * cont = new TaskImageContainer(data, signal);
		return cont;
	}
	return 0;
}
// Load opengl data
void ImageData::load_data(){
	int my_state=State::Hell;
	{
	  boost::mutex::scoped_lock state_lock(state_mutex);

	  if(state == State::ImlibReady){
	  		my_state = State::ImlibReady;
			state = State::Used;
	  }
	}
	if(my_state == State::ImlibReady){
		try{
			data = new TextureData(*imlib_data);
		}
		catch(std::exception){
			boost::mutex::scoped_lock state_lock(state_mutex);
			data=0;
			state = State::ImlibReady;
			throw;
		}
		age =0;
	 	boost::mutex::scoped_lock state_lock(state_mutex);
	 	state = State::Ready;
	}
}

// load imlib data
void ImageData::load_imlib_data(){
	int my_state=State::Hell;
	{
	  boost::mutex::scoped_lock state_lock(state_mutex);

	  if(state == State::Empty){
	  		my_state = State::Empty;
			state = State::Used;
	  }
	}
	if(my_state == State::Empty){
		try{
			imlib_data = new ImlibData(name);
		}
		catch(std::exception){
			boost::mutex::scoped_lock state_lock(state_mutex);
			imlib_data = 0;
			state = State::Error;
			throw;
		}
		age =0;
	 	boost::mutex::scoped_lock state_lock(state_mutex);
	 	state = State::ImlibReady;
	}
}

void ImageData::delete_imlib_data(){
	int my_state=State::Hell;
	{
	  boost::mutex::scoped_lock state_lock(state_mutex);

	  if(state == State::ImlibReady){
	  		my_state = state;
			state = State::Used;
	  }
	}
	if(my_state == State::ImlibReady){
		if(global_options->get_verbose()) {
			boost::mutex::scoped_lock io_lock(global_iomutex);
			cerr << "Delete imlib data: " << name << endl;
		}
		delete imlib_data;
		imlib_data=0;
		boost::mutex::scoped_lock state_lock(state_mutex);
		state = State::Empty;
	}
}
void ImageData::delete_data(){
	int my_state=State::Hell;
	{
	  boost::mutex::scoped_lock state_lock(state_mutex);

	  if(state == State::Ready){
	  		my_state = state;
			state = State::Used;
	  }
	}
	if(my_state == State::Ready){
		if(global_options->get_verbose()) {
			boost::mutex::scoped_lock io_lock(global_iomutex);
			cerr << "Delete GL data: " << name << endl;
		}
		delete data;
		data=0;
		boost::mutex::scoped_lock state_lock(state_mutex);
		state = State::ImlibReady;
	}
}
void ImageData::delete_all(){
	int my_state=State::Hell;
	{
	  boost::mutex::scoped_lock state_lock(state_mutex);

	  if(state == State::Ready || state == State::ImlibReady){
	  		my_state = state;
			state = State::Used;
	  }
	}
	if(my_state == State::Ready || my_state == State::ImlibReady){
		if( my_state == State::Ready ){
			if(global_options->get_verbose()) {
				boost::mutex::scoped_lock io_lock(global_iomutex);
				cerr << "Delete GL data: " << name << endl;
			}
			delete data;
			data=0;
		}
		if(global_options->get_verbose()) {
			boost::mutex::scoped_lock io_lock(global_iomutex);
			cerr << "Delete imlib data: " << name << endl;
		}
		delete imlib_data;
		imlib_data=0;
		boost::mutex::scoped_lock state_lock(state_mutex);
		state = State::Empty;
	}
}

// create opengl texture
TextureData::TextureData(const ImlibData & data): w(data.w), h(data.h), texture_size(data.texture_size){
	xt1=0;
	yt1=1 -( GLfloat ) data.h / ( GLfloat ) data.texture_size;
	xt2=( GLfloat ) data.w / ( GLfloat ) data.texture_size;
	yt2=1;

	glGenTextures( 1, &texture );
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, texture_size, texture_size , 0, GL_RGBA, GL_UNSIGNED_BYTE, data.data);
	
	// handle errors
	GLenum errCode = glGetError();
	if (errCode != GL_NO_ERROR) {
		if(errCode == GL_OUT_OF_MEMORY){
			glDeleteTextures(1, &texture);
			throw std::bad_alloc();
		}
		else{
			glDeleteTextures(1, &texture);
			throw std::runtime_error("GL error.");
		}
	}
}

// loads the image from the disk using imlib library
ImlibData::ImlibData(const string & name){
	Imlib_Image im_image;
	{
		size_t found=name.find(':');
		if(found != string::npos)
			throw std::logic_error("':' found in the image name.");
	}

	im_image = imlib_load_image(name.c_str());
	if( ! im_image )
		throw std::runtime_error("Could not load image.");
	
    imlib_context_set_image(im_image);
	// OPENGL reads it backwards
    imlib_image_flip_vertical();

	w = imlib_image_get_width();
	h = imlib_image_get_height(); 
	texture_size = global_options->get_max_texture_size();

	if ( (w < texture_size) && (h < texture_size) ){
		// find nearest bigger power of two */
		int bigger = w > h ? w : h;
		int n=1;
		if( bigger & (bigger-1) ){ // if bigger is not power of 2
			while( bigger >>= 1 ) ++n;
			texture_size = 1 << n;
		}else
			texture_size = bigger;
	}
	// create opengl texture- size and format ( RGBA ) - compatible image

	data = new DATA32[texture_size * texture_size];

	// Paint It Black ( alpha channel 255 )
	for( int i=0; i < texture_size ; i++)
		for( int j=0; j < texture_size ; j++){
			int c = (((i&0x8)==0)^((j&0x8) == 0))*64 + 64;
			data[ i * texture_size + j] = 0xFF000000 | c<<16 | c<<8 | c;
		}

	Imlib_Image im_new_image = imlib_create_image_using_data( texture_size, texture_size, data );
	if( ! im_new_image )
		throw std::runtime_error("Imlib error. ( should not happen ).");

    imlib_context_set_image( im_new_image );
	// compute coordinates and scale ratio
	// place the texture to the bottom left corner
	int y, width, height;
	if( w >= h ){
		if( w > texture_size ){
			width = texture_size; height = ( int ) width * h/w;
			y = ( int )(texture_size - height);
		}else{
			y=(int)(texture_size - h); width=w; height = h;
		}
	}else{
		if( h > texture_size ){
			height = texture_size; width = ( int ) height * w/h;
			y =0;
		}else{
			y=(int)(texture_size - h); width=w; height = h;
		}
	}
	// scale original image onto new one
	{	
		int malpha = 0xFF;
		imlib_blend_image_onto_image( im_image, malpha, 0,0, w,h, 0,y, width,height );
	}
	// free the old one
	imlib_context_set_image( im_image );
	imlib_free_image();
	
	imlib_context_set_image( im_new_image );
	
	DATA32 blue,red;
	data = imlib_image_get_data();

	/* convert data from BGRA to RGBA (from least to most significant bit) */
	/*
		FROM 0xAARRGGBB
		TO	 0xAABBGGRR
	*/
	for(int i=0; i < texture_size * texture_size ; i++ ){
		red =  ( data[i] >> 16 ) & 0x000000FF;
		blue = ( data[i] << 16 ) & 0x00FF0000;
		data[i] = ( data[i] & 0xFF00FF00 ) | blue | red;
	}

	imlib_image_put_back_data(data);
	w=width;
	h=height;
}

