#ifndef _MAIN_OBJH
#define _MAIN_OBJH

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/xtime.hpp>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <FTGL/ftgl.h>
#include <iostream>
//#include <string>
//#include <algorithm>
#include <vector>
#include <stdexcept>
#include <time.h>
#include <stdlib.h>

#include "threading.h"
#include "image.h"
#include "options.h"
#include "job_manager.h"
#include "image_face.h"
#include "thumbnail_face.h"
#include "info_face.h"

class Main{
public:
	Main( std::vector<Slide *> & slds, TaskMutexQueue & tmq, TaskMutexQueue & res_tmq );
	/*		Glut functions called from main.cpp		*/
	/*		See main.cpp and some glut tutorial.	*/
	void glut_reshape_window(int w, int h);
	void glut_render_scene(void);
	void glut_idle_func(void);
	void glut_special_keyboard_event(int key, int, int);
	void glut_normal_keyboard_event(unsigned char key, int, int);
private:
	// exit & sleep
	void my_exit();
	void sleep(unsigned ms);

	//movement and transitions
	void move_next();
	void move_prev();
	/*		Transition is a smooth rotating movement to the next		*/
	/*		or prev image or to or from thumbnail face			*/
	void transition_left();
	void transition_right();
	void transition_to_thumbnails();
	void transition_from_thumbnails();

	/*		Updates the rotation flag		*/
	void do_transition();
	/*		If we e.g. move left and the user preses right		*/
	/*		We want to stop the movement left and start the		*/
	/*		movement right from the point we ended moving		*/
	/*		left. This is done by setting the time "backward"	*/
	void reverse_time_in_transition();
	GLfloat rotation;
	boost::xtime xt_rotation_start;

	/*		In slideshow we want to simulate user pressing		*/
	/*		next button every now end then (when the time		*/
	/*		from the beginning ( xt_slideshow_start ) is big	*/
	/*		enough.												*/
	void do_slideshow();
	bool slideshow;
	boost::xtime xt_slideshow_start;

	/*		Proximity functions return indexes of images close	*/
	/*		to the current slide. Used to order images around	*/
	/*		the current one.	*/
	int proximity_right();
	int proximity_left();
	/*		the index of the current image				*/
	unsigned current_slide;
	/*		the last move direction, either 1 or -1 	*/
	int move_direction;

	/*		Keyboard actions		*/
	void keyboard_event(Const::Keys::KeyReturn ret);
	void rotate_current(unsigned rot);
	void toggle_marked_current();
	void toggle_slideshow();
	void toggle_info();

	// loading images
	void load_gl_data();
	bool load_checked(int index, int priority);
	bool load_checked(int index);
	void image_view_order_images();

	/*		Aging and old data removal			*/
	/*		Ensures that we free images that	*/
	/*		were not used recently				*/
	void do_aging();
	bool age_image(ImageData * image, unsigned at_age );
	/*		Function that forcely removes		*/
	/*		old images. (when e.g bad_alloc		*/
	void force_remove_old();
	void check_old(ImageData * image, unsigned & age_old, ImageData * & image_old);
	boost::xtime xt_last_aging;


	/*		Data		*/
	std::vector<Slide *> & slides;		// vector of slides
	TaskMutexQueue & job_queue;			// queue of task for loading thread 
	TaskMutexQueue & result_queue;		// queue of results from ---||---
	JobManager job_manager;				// class managing image ordering
	FTFont* ft_font;					// font drawing class

	/*		Faces that do the drawing		*/
	ImageFace * imf;
	ImageFace * imf_left;
	ImageFace * imf_right;
	ThumbnailFace * tmf;
	InfoFace * inff;
	
	/*		Flag saying how "verbose" are we		*/
	/*		in showing info							*/
	unsigned show_info;
	Const::ShowState show_state;
	int screen_width, screen_height;

	// stats
	unsigned long time_all, time_draw, time_gl, time_aging;
};


#endif
