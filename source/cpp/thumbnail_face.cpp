#include "thumbnail_face.h"

extern Options * global_options;

ThumbnailFace::ThumbnailFace(std::vector<Slide *> & slds, JobManager & jbm) : slides(slds),
				job_manager(jbm),
				current_slide(0),current_screen_beg(0), slides_num_w(0),
				slides_num_h(0), one_screen(0) {
					null_pos = new Position;
				}

ThumbnailFace::~ThumbnailFace(){
	delete null_pos;
}
// orders images around the current one, from first to last
void ThumbnailFace::order_images(int first, int last){
	int two_times_current = 2 * current_slide;

	if(first > last)
		throw std::runtime_error("first > last") ;
		
	if( first <= (int) current_slide && (int) current_slide <= last ){
		if( last - current_slide >= current_slide - first){
			for(int i = last ; two_times_current- first < i ; i--)
				load_checked(i);
			for(int i = current_slide - first ; i > 0 ; i -- ){
				load_checked(current_slide + i);
				load_checked(current_slide - i);
			}
			load_checked(current_slide);
		}else{
			for(int i = first ; two_times_current - last > i ; i++)
				load_checked(i);
			for(int i = last - current_slide ; i > 0 ; i -- ){
				load_checked(current_slide + i);
				load_checked(current_slide - i);
			}
			load_checked(current_slide);
		}
	}else
		for(int i = last ; first <= i ; i--)
			load_checked(i);
}

// order all thumbnails that are shown
void ThumbnailFace::order_necessary_images(){
	order_images(current_screen_beg, current_screen_beg + one_screen - 1);
}
// order thumbnails, with correct index
bool ThumbnailFace::load_checked(int index, int priority){
	if( index >=0 && index < (int) positions.size()){
		if( slides[index]->thumbnail )
			return job_manager.order_image(slides[index]->thumbnail, priority);
		else
			return job_manager.order_image(slides[index]->image, priority);
	}
	return 0;
}
bool ThumbnailFace::load_checked(int index){
	if( index >=0 && index < (int) positions.size()){
		if( slides[index]->thumbnail )
			return job_manager.order_image(slides[index]->thumbnail);
		else
			return job_manager.order_image(slides[index]->image);
	}
	return 0;
}

void ThumbnailFace::update_screen_beg(){
	// if we moved forward from the screen
	if(current_slide >= current_screen_beg + one_screen){
		if(current_slide - (current_screen_beg + one_screen) < slides_num_h){
			current_screen_beg += slides_num_h;
		}else
			compute_new_screen_beg();
	}
	// if we moved backward from the screen
	if(current_slide < current_screen_beg){
		if(current_screen_beg - current_slide <= slides_num_h){
			current_screen_beg -= slides_num_h;
		}else
			compute_new_screen_beg();
	}
}
void ThumbnailFace::compute_new_screen_beg(){
	if( current_slide < one_screen){
		current_screen_beg = 0; //current_slide - current_slide % slides_num_h;
	}	
	else{
		current_screen_beg = current_slide - current_slide % slides_num_h - one_screen +slides_num_h;
	}
}


// MOVEMENT functions

void ThumbnailFace::move_up(){
	move_up(1);
}
void ThumbnailFace::move_up(bool order){
	if(current_slide>0){
		current_slide--;
		update_screen_beg();
		if(order)order_images(current_screen_beg, current_screen_beg + one_screen-1);
	}
}
void ThumbnailFace::move_down(){
	move_down(1);
}
void ThumbnailFace::move_down(bool order){
	if(current_slide<slides.size()-1){
		current_slide++;
		update_screen_beg();
		if(order)order_images(current_screen_beg, current_screen_beg + one_screen-1);
	}else
	if( current_slide == slides.size()-1 && (current_slide+1) % slides_num_h != 0 && current_slide >= slides_num_h ){
		current_slide = current_slide - slides_num_h + 1;
		update_screen_beg();
		if(order)order_images(current_screen_beg, current_screen_beg + one_screen-1);
	}
}
void ThumbnailFace::move_left(){
	if(current_slide > slides_num_h-1){
		current_slide -= slides_num_h;
		update_screen_beg();
		order_images(current_screen_beg, current_screen_beg + one_screen-1);
	}
}
void ThumbnailFace::move_right(){
	if(current_slide + slides_num_h <= slides.size()-1){
		current_slide += slides_num_h;
		update_screen_beg();
		order_images(current_screen_beg, current_screen_beg + one_screen-1);
	} else
	if( current_slide / slides_num_h !=  (slides.size()-1) / slides_num_h){
		current_slide = slides.size()-1;
		update_screen_beg();
		order_images(current_screen_beg, current_screen_beg + one_screen-1);
	}

}
void ThumbnailFace::move_end(){
	current_slide = slides.size() - 1;
	update_screen_beg();
	order_images(current_screen_beg, current_screen_beg + one_screen-1);
}
void ThumbnailFace::move_home(){
	current_slide = 0;
	update_screen_beg();
	order_images(current_screen_beg, current_screen_beg + one_screen-1);
}
// movement fcs. used by MainObject
void ThumbnailFace::move_prev(){
	if(current_slide > 0 )
		move_up(0);
}
void ThumbnailFace::move_next(){
	if(current_slide < slides.size() - 1)
		move_down(0);
}

unsigned ThumbnailFace::get_current(){
	return current_slide;
}

// draws selection box around the thumbnail position
void ThumbnailFace::draw_current_selection(const Position * pos){
		glColor3f(0.3f, 0.3f, 0.5f);
		glBegin (GL_POLYGON);
			 glVertex3f( pos->x1 - border_in_gl_units / 2, pos->y1 - border_in_gl_units / 2,  pos->z - 0.0001);
			 glVertex3f( pos->x2 + border_in_gl_units / 2, pos->y1 - border_in_gl_units / 2,  pos->z - 0.0001);
			 glVertex3f( pos->x2 + border_in_gl_units / 2, pos->y2 + border_in_gl_units / 2,  pos->z - 0.0001);
			 glVertex3f( pos->x1 - border_in_gl_units / 2, pos->y2 + border_in_gl_units / 2,  pos->z - 0.0001);
		glEnd();
}

void ThumbnailFace::draw(unsigned){
	if(current_screen_beg > current_slide || current_screen_beg + one_screen < current_slide)
		throw std::runtime_error("screen beg and current slide misshape") ;

	glPushMatrix();
	glTranslatef(startx, starty,0);

	int k = 0;
	for( unsigned i = current_screen_beg; i < slides.size() && i < current_screen_beg + one_screen ; i++){
		glPushMatrix();
		glTranslatef(stepx* ((int) k / slides_num_h),-stepy * ((int) k % slides_num_h), 0);
		glRotatef(slides[i]->rot, 0.0, 0.0, 1.0);
		if(i == current_slide){
			if(image_ready(slides[i]->thumbnail) || image_ready(slides[i]->image))
				draw_current_selection(positions[i]);
			else
				draw_current_selection(null_pos);
		}
		if(image_ready(slides[i]->thumbnail))
			draw_image(slides[i]->thumbnail, positions[i]);
		else if(image_ready(slides[i]->image))
			draw_image(slides[i]->image, positions[i]);
		glPopMatrix();
		k++;
	}
	glPopMatrix();
}

// reshape window so that the thumbnails show fit it correctly and nicely
void ThumbnailFace::reshape(int w, int h){
	if(positions.empty())
		for(unsigned i = 0; i < slides.size(); i++){
			Position * pom = new Position;
			positions.push_back(pom);
		}
	screen_w = w;
	screen_h = h;

	int min_size = global_options->get_min_thumbnail_size();
	int border = global_options->get_border_size();

	slides_num_h = h / min_size;
	if(slides_num_h == 0) slides_num_h=1;
	slides_num_w = w / min_size;
	if(slides_num_w == 0) slides_num_w=1;

	int image_w = (w - border)/ slides_num_w - border;
	int image_h = (h - border)/ slides_num_h - border;

	GLfloat ratio = (GLfloat) image_w/w;
	for(unsigned i=0 ; i< slides.size() ; i++){
		ImageData * thumb = 0;
		if(image_ready(slides[i]->thumbnail))
			thumb =  slides[i]->thumbnail;
		else if(image_ready(slides[i]->image))
			thumb =  slides[i]->image;

		if(thumb){
			image_reshape(positions[i], slides[i]->rot, thumb->data->w, thumb->data->h, image_w, image_h);
			positions[i]->x1 *= ratio;
			positions[i]->y1 *= ratio;
			positions[i]->x2 *= ratio;
			positions[i]->y2 *= ratio;
		}
	}
	border_in_gl_units = (GLfloat) 2 * border / w;
	GLfloat  image_w_in_gl_units = (GLfloat) 2 * image_w /(GLfloat)  w;
	GLfloat  image_h_in_gl_units = (GLfloat) 2 * image_h /(GLfloat) w;

	stepx = border_in_gl_units + image_w_in_gl_units;
	stepy = border_in_gl_units + image_h_in_gl_units;

	startx = border_in_gl_units + image_w_in_gl_units / 2- (GLfloat) 1;
	starty = (GLfloat) h/w - border_in_gl_units - image_h_in_gl_units / 2;
	if(slides_num_h * slides_num_w != one_screen){
		one_screen = slides_num_h * slides_num_w;
		compute_new_screen_beg();
		order_images(current_screen_beg, current_screen_beg + one_screen-1);
	}
}
