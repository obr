#ifndef _INFO_FACE
#define _INFO_FACE

#include <GL/gl.h>
#include <FTGL/ftgl.h>
#include <stdexcept>
#include <iostream>
#include <string>

#include "image.h"
#include "options.h"
#include "face.h"

// Face showing text information like slideshow state or image position
class InfoFace: public Face{
public:
	InfoFace(FTFont* ft );
	~InfoFace();
	/*		Face API		*/
	virtual void draw(unsigned info);
	virtual void reshape(int w, int h);
	/*		Used to update the text		*/
	/*		that the face is showing	*/
	void update(unsigned current_slide, unsigned slides_total, bool _slideshow);
private:
	bool slideshow;
	/*		Text we draw				*/
	std::string info_text;

	/*		Object drawing texts		*/
	FTFont * ft_font;

	/*		Positions of texts			*/
	FTPosition * info_pos;
	FTPosition * slide_pos;
};
#endif
