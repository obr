#ifndef _FACE
#define _FACE

#include <GL/gl.h>
#include <FTGL/ftgl.h>
#include <stdexcept>

#include "image.h"
#include "options.h"
/*		 basic abstract Face class		*/
class Face{
public:
	virtual void draw(unsigned info) = 0;
	virtual void reshape(int w, int h) = 0;
	// struct for image position
	struct Position{
		void null(){x1=0.0;x2=0.0;y1=0.0;y2=0.0;}
		Position():z(1.0) {null();}
		Position(GLfloat _z):z(_z) {null();}
		GLfloat x1,y1,x2,y2,z;
	};
	// struct for text position
	struct FTPosition{
		GLfloat x,y,z;		// coor for the text itself
		GLfloat xs,ys,zs;	// coor for the shadow underneath
		int face_size;
	};
	// compute image position `pos' so it fits in the screen `screen_width/height' the best it could
	void image_reshape( Position * pos, int rotation, int image_width, int image_height, int screen_width, int screen_height );
	// draws image to position
	void draw_image(ImageData * image, Position * pos);
	// draws colored text to position, using font clas FTFont
	void draw_text(FTFont * font, FTPosition * text_pos, const char * text, GLfloat red, GLfloat green, GLfloat blue);
	// predicate for testing whether image is ready (its gl texture is loaded)
	bool image_ready(ImageData * image);
	// predicate for testing whether image data (imlib) is loaded from the disk
	bool image_imlib_ready(ImageData * image);

};
#endif
