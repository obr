#include "threading.h"

#include <iostream>
/*
#include "options.h"
using std::cerr;
using std::endl;

extern boost::mutex global_iomutex;
*/
void ThreadWorker::operator()(){
	for(;;){
		if(!in_job_queue.empty()){
			job_num++;
        	TaskMutexQueue::value_type pair = in_job_queue.get_pair();

			int priority = pair.first;
/*
			if(global_options->get_verbose()) {
				boost::mutex::scoped_lock io_lock(global_iomutex);
				cerr << "Job with priority: " << priority << endl;
			}
*/
			Task * t = pair.second;

			if(t->signal() == Signal::Quit)
				return;
			//try{
				Task * result = t->do_it();
				if(result)
					out_job_queue.push(result, priority);
			//}
			//catch(std::exception){ }
		}else
			sleep();
	}
}
void ThreadWorker::sleep(void){
	boost::xtime xt;
	boost::xtime_get(&xt, boost::TIME_UTC);
	xt.nsec += 50000000;
	boost::thread::sleep(xt);
}
