#ifndef _PARSERH
#define _PARSERH

#include <boost/tokenizer.hpp>
#include <iostream>
#include <istream>
#include <string>
#include <vector>

#include "options.h"
#include "image.h"

typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;

/*		Class parsing the input			*/
class Parser{
public:
	/*		Parse from the in_stream	*/
	Parser(std::istream & in_stream): in(in_stream) {};
	/*		Save to options, and vector	*/
	/*		of slides					*/
    void operator()( Options *options, std::vector<Slide *> *slide_vector);
private:
	/*		Gets 4 tokens from the 		*/
	/*		in stream					*/
    Tokenizer * get_tokens();
	/*		Reads a number from the 	*/
	/*		in stream					*/
	unsigned read_num();
		
	std::istream & in;
};

#endif
