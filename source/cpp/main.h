#ifndef _MAINH
#define _MAINH
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#include <list>
#include <vector>
#include <stdexcept>

#include "threading.h"
#include "image.h"
#include "input_parser.h"
#include "options.h"
#include "main_obj.h"

#endif
