#include "face.h"

bool Face::image_ready(ImageData * image){
	if(!image)
		return 0;
	return image->test_ready();
}
bool Face::image_imlib_ready(ImageData * image){
	if(!image)
		return 0;
	return image->test_imlib_ready();
}

// reshape image coordinates so that it fits screen the best it could
void Face::image_reshape( Position * pos, int rotation, int image_width, int image_height, int screen_width, int screen_height	){
	if(!pos)
		throw std::logic_error("Zero *pos");

	GLuint two_units = screen_width;
	if( rotation % 180 ){
		// this trick allows easily computing of coordinates
		// for '90' turning
		screen_width = screen_height;
		screen_height = two_units ;
	}

	GLfloat image_ratio = ( GLfloat ) image_height / ( GLfloat ) image_width;
	GLfloat screen_ratio = ( GLfloat ) screen_height / ( GLfloat ) screen_width;

	// if the image fits the screen
	if( (image_ratio <= screen_ratio && screen_width >= image_width) || (image_ratio > screen_ratio  && screen_height >= image_height) ){
			pos->x2 = (GLfloat ) ( image_width ) / (GLfloat ) two_units ;
			pos->x1 = - pos->x2;
			pos->y2 = (GLfloat ) ( image_height ) / (GLfloat ) two_units ;
			pos->y1 = - pos->y2;
	}else
	// if it is limited by width (and width is bigger)
	if(image_ratio <= screen_ratio && screen_width < image_width){
			// C'mon lets scale !
			pos->x2 = (GLfloat ) screen_width / (GLfloat ) two_units;
			pos->x1 = -pos->x2;
			pos->y2 = pos->x2 * image_ratio;
			pos->y1 = - pos->y2;
	}else
	// if it is limited by height (and width is the bigger)
	if(image_ratio > screen_ratio && screen_height < image_height){
			// C'mon lets scale !
			pos->y2 = (GLfloat) screen_height / (GLfloat) two_units;
			pos->y1 = -pos->y2;
			pos->x2 = pos->y2 / image_ratio ;
			pos->x1 = -pos->x2;
	}else
		throw std::runtime_error("Else???");
}
void Face::draw_image(ImageData * image, Position * pos){
	if(!pos)
		return;
	if( image_ready(image) ){
		TextureData * data = image->data;
		image->age = 0;

		glCullFace(GL_BACK);
		glEnable (GL_CULL_FACE);

		glEnable (GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, data->texture);
		glBegin(GL_QUADS);
			glTexCoord2f(data->xt1, data->yt1); glVertex3f( pos->x1, pos->y1,  pos->z);
			glTexCoord2f(data->xt2, data->yt1); glVertex3f( pos->x2, pos->y1,  pos->z);
			glTexCoord2f(data->xt2, data->yt2); glVertex3f( pos->x2, pos->y2,  pos->z);
			glTexCoord2f(data->xt1, data->yt2); glVertex3f( pos->x1, pos->y2,  pos->z);
		glEnd();
		glDisable (GL_TEXTURE_2D);
		glDisable (GL_CULL_FACE);
	}
}
void Face::draw_text(FTFont * font, FTPosition * text_pos, const char * text, GLfloat red, GLfloat green, GLfloat blue){
	if(!font)
		throw std::logic_error("No font.");
	if(!text_pos)
		throw std::logic_error("No pos.");

	font->FaceSize(text_pos->face_size);

	glColor3f(0.0,0.0,0.0);
	glRasterPos3f(text_pos->xs, text_pos->ys, text_pos->zs);
	font->Render(text);

	glColor3f(red, green, blue);
	glRasterPos3f(text_pos->x, text_pos->y, text_pos->z);
	font->Render(text);

}
