#ifndef _IMAGE_FACE
#define _IMAGE_FACE

#include <GL/gl.h>
#include <FTGL/ftgl.h>
#include <stdexcept>

#include "image.h"
#include "options.h"
#include "face.h"

/*		 Face drawing an image, its histogram and text		*/
class ImageFace: public Face{
public:
	ImageFace(Slide * sld, FTFont* ft );
	~ImageFace();
	/*		Face API		*/
	virtual void draw(unsigned info);
	virtual void reshape(int w, int h);
private:
	/*		Slide that contains the data	*/
	Slide * slide;

	/*		Object drawing texts		*/
	FTFont * ft_font;

	/*		Positions of texts			*/
	FTPosition * ft_pos;		// text position
	FTPosition * marked_pos;	// marked flag position
	/*		Positions of textures			*/
	Position * image_pos;		// image position
	Position * histogram_pos;	// histogram postition
};
#endif
