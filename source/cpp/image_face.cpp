#include "image_face.h"

extern Options * global_options;

ImageFace::ImageFace(Slide * sld, FTFont * ft): slide(sld),ft_font(ft), ft_pos(0), marked_pos(0), image_pos(0), histogram_pos(0) { }

ImageFace::~ImageFace(){
	delete ft_pos;
	delete marked_pos;
	delete histogram_pos;
	delete image_pos;
}
void ImageFace::draw(unsigned info){
	if(!slide)
		throw std::runtime_error("ImageFace: zero slide.");	
	// draw the image 
	glPushMatrix();
		// with correct rotation
		glRotatef(slide->rot, 0.0, 0.0, 1.0);
		// if we do not have image, we use thumbnail instead (if we have at least it)
		if(image_ready(slide->image))
			draw_image(slide->image, image_pos);
		else if( image_ready(slide->thumbnail))
			draw_image(slide->thumbnail, image_pos);
	glPopMatrix();
	// draw info
	// marked flag
	if(info & 1)
		if(slide->marked)
			draw_text(ft_font, marked_pos, Const::DefaultMarkedText.c_str(), 0.8, 0.1, 0.1);
	// histogram
	if(info & 4)
		draw_image(slide->histogram, histogram_pos);
	// description
	if(info & 2)
		if( slide->text )
			draw_text(ft_font, ft_pos, slide->text->c_str(), 1.0,1.0,1.0);
}

// compute text coordinates so that it fits in the window
void ImageFace::reshape(int w, int h){
	if(!slide)
		throw std::runtime_error("ImageFace: zero slide.");	
	// of the image
	if(image_ready(slide->image)){
		if(!image_pos) image_pos = new Position;
		image_reshape(image_pos, slide->rot, slide->image->data->w+8, slide->image->data->h+8, w,h);
		GLfloat shift=(GLfloat)4/w;
		image_pos->x1 += shift; image_pos->x2 -= shift;
		image_pos->y1 += shift; image_pos->y2 -= shift;
	// of the thumbnail if we use it instead of image
	}else if( image_ready(slide->thumbnail)){
		if(!image_pos) image_pos = new Position;
		if( image_imlib_ready(slide->image))
			image_reshape(image_pos, slide->rot, slide->image->imlib_data->w+8, slide->image->imlib_data->h+8, w,h);
		else
			image_reshape(image_pos, slide->rot, w+8, w * slide->thumbnail->data->h / slide->thumbnail->data->w + 8, w,h);
		GLfloat shift=(GLfloat)4/w;
		image_pos->x1 += shift; image_pos->x2 -= shift;
		image_pos->y1 += shift; image_pos->y2 -= shift;
	}
	// of the histogram
	if(image_ready(slide->histogram)){
		if(!histogram_pos) histogram_pos = new Position;
		image_reshape(histogram_pos, 0, slide->histogram->data->w+8, slide->histogram->data->h+8, w,h);
		histogram_pos->z=1.001;
		GLfloat dx =  histogram_pos->x2 -  histogram_pos->x1;
		GLfloat dy =  histogram_pos->y2 -  histogram_pos->y1;
		
		histogram_pos->x1 = - 1;
		histogram_pos->y2 = (GLfloat) h / w;	
		histogram_pos->x2 = histogram_pos->x1 + dx;
		histogram_pos->y1 = histogram_pos->y2 - dy;
		
		GLfloat shift=(GLfloat)4/w;
		histogram_pos->x1 += shift; histogram_pos->x2 -= shift;
		histogram_pos->y1 += shift; histogram_pos->y2 -= shift;
	}
	int def_ft_size = global_options->get_font_size();
	// of the text
	if( slide->text ){
		if(!ft_pos) ft_pos = new FTPosition;
		if(def_ft_size >= h/8)
			ft_pos->face_size=h/8;
		else
			ft_pos->face_size=def_ft_size;

		ft_font->FaceSize(ft_pos->face_size);
		float width_of_the_text = (GLfloat) ft_font->Advance(slide->text->c_str())/w + 8/w;
		if(width_of_the_text> 1.0){
			ft_pos->face_size = (int) (ft_pos->face_size / width_of_the_text);
			ft_pos->x= - 1+4/w;
		}
		else
			ft_pos->x= - (GLfloat) width_of_the_text;
		ft_pos->y= - (GLfloat) h / w + (GLfloat) ft_pos->face_size / w + 4 /w ;	
		ft_pos->z=1.01;
		ft_pos->xs=ft_pos->x + (GLfloat) ft_pos->face_size  /(8 * w);
		ft_pos->ys=ft_pos->y + (GLfloat) ft_pos->face_size  /(10 * w);
		ft_pos->zs=1.009;
	}
	// of the marked flag
	{
		if(!marked_pos) marked_pos = new FTPosition;
		if(def_ft_size >= h/8)
			marked_pos->face_size=h/8;
		else
			marked_pos->face_size=def_ft_size;
		ft_font->FaceSize(marked_pos->face_size);
		float width_of_the_text = (GLfloat) 2*ft_font->Advance(Const::DefaultMarkedText.c_str())/w +(float) 8/w;
		if(width_of_the_text> 1.0){
			marked_pos->face_size = (int) (marked_pos->face_size /width_of_the_text);
			marked_pos->x= 0;
		}
		else
			marked_pos->x= 1-width_of_the_text;
		marked_pos->y= (GLfloat) h / w - (GLfloat) 4*marked_pos->face_size / w ;//- 4 /w ;	
		marked_pos->z=1.01;
		marked_pos->xs=marked_pos->x + (GLfloat) marked_pos->face_size /(8 * w);
		marked_pos->ys=marked_pos->y + (GLfloat) marked_pos->face_size /(10 * w);
		marked_pos->zs=1.009;
	}
}
