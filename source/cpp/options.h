#ifndef _OPTIONSH
#define _OPTIONSH

#include <GL/glut.h>
#include <boost/thread/mutex.hpp>
#include <string>
#include <iostream>

using std::string;
/*		Global namespace containing constants		*/
/*		And basically all the default options		*/
/*		values.										*/
namespace Const{
	/*		Defaul options params		*/
	enum{
		DefaultTextureSize=1024 ,
		DefaultSlideshowDelay=1000,
		DefaultFontSize=48,
		DefaultBorderSize=40,
		DefaultThumbnailMinSize=256,
		DefaultRotationDelay=300
	};
	const string DefaultFont =  "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf";
	/*		Default text showed when		*/
	/*		the image is marked by user.	*/
	const string DefaultMarkedText = "M";
	const string DefaultSlideshowText = "S";

	enum TransitionEffect{
		TransitionSmooth, TransitionSkip
	};
	enum MemoryPolicy{
		MemoryAggressive
	};
	enum ShowState{
		ThumbnailView,ImageView, EditText,
		SmoothTransitionLeft, SmoothTransitionRight,
		SmoothTransitionToThumbnailView, SmoothTransitionFromThumbnailView
	};
	namespace Keys{
		enum Keycodes{
			Left, Right, Up, Down, Home, End,
			Slideshow, Fullscreen, Info, Thumbnails,
			Mark, Escape, Space, Enter, Backspace, Delete,
			Unknown
		};
		struct KeyReturn{
			Keycodes keycode;
			unsigned char key;
			KeyReturn():keycode(Unknown), key(0) {}
		};
	}
}
/*		Class holding global options				*/
/*		it is mutexed to allow for concurrency		*/
class Options{
public:
#define GET_SET(TYPE,NAME)\
TYPE get_##NAME () {\
		boost::mutex::scoped_lock options_lock(mutex);\
		return NAME;}\
void set_##NAME (TYPE new_value){\
		boost::mutex::scoped_lock options_lock(mutex);\
		NAME=new_value;}

	GET_SET(unsigned, max_texture_size)
	GET_SET(unsigned, slideshow_delay)
	GET_SET(unsigned, font_size)
	GET_SET(unsigned, border_size)
	GET_SET(unsigned, min_thumbnail_size)
	GET_SET(unsigned, rotation_delay)
	GET_SET(bool, verbose)
	GET_SET(Const::TransitionEffect, transition_effect)
	//GET_SET(Const::MemoryPolicy, memory_policy)
	GET_SET(const string &, font)

#undef GET_SET
	/*		In the future we would like to			*/
	/*		allow for user-defined keys setup.		*/
	Const::Keys::KeyReturn translate_from_glut_normal_keycode(unsigned char key);
	Const::Keys::KeyReturn translate_from_glut_special_keycode(int key);

	/*		Print the options to the stdout		*/
    void print();

	/*		The constructor 	*/
    Options():  
                max_texture_size(Const::DefaultTextureSize),
				slideshow_delay(Const::DefaultSlideshowDelay),
                border_size(Const::DefaultBorderSize),
                min_thumbnail_size(Const::DefaultThumbnailMinSize),
                rotation_delay(Const::DefaultRotationDelay),
				verbose(0),
				transition_effect(Const::TransitionSkip),
			//	transition_effect(Const::TransitionSmooth),
                //memory_policy(Const::MemoryAggressive),
                font(Const::DefaultFont), font_size(Const::DefaultFontSize){}
private:
	/*		Maximal size of the GL texture		*/
	/*		bigger images will be scaled 		*/
	/*		resulting in faster execution,		*/
	/*		and lower mem usage					*/
	/*		Must be a power of 2!!				*/
    unsigned max_texture_size;	
	/*		Time in ms, between slideshow transitions		*/
	unsigned slideshow_delay;
	/*		Size of border around images in thumbnail view	*/
	unsigned border_size;
	/*		Minimal size of thumbnails, if they	cannot		*/
	/*		fit in the screen we smaller the amount of		*/
	/*		them on the screen. See thumbnail_face.h		*/
	unsigned min_thumbnail_size;
	/*		How long should a smooth transition's rotation	*/
	/*		take?				*/
	unsigned rotation_delay;
	/*		Be verbose?? :-)	*/
	bool verbose;

	/*		What should the transition be?		*/
	/*		TransitionSmooth (gl cube effect)	*/
	/*	 or TransitionSkip (instant, no eff.)	*/
	Const::TransitionEffect transition_effect;
	
	/*		Font name to be used		*/
    string font;
	/*		And its default size		*/
	unsigned font_size;

    boost::mutex mutex;
};
#endif
