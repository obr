#include "main.h"

//global mutex for locking input & output
boost::mutex	global_iomutex;
Options	*		global_options;

// namespace to "encapsulate" glut functions
namespace global_glut{
	Main * main_obj;

	void reshape_window(int w, int h){ main_obj->glut_reshape_window(w, h); }
	void render_scene(void){ main_obj->glut_render_scene(); }
	void special_keyboard_event(int key, int mouse_x, int mouse_y) { main_obj->glut_special_keyboard_event(key, mouse_x, mouse_y); }
	void normal_keyboard_event(unsigned char key, int mouse_x, int mouse_y) { main_obj->glut_normal_keyboard_event(key, mouse_x, mouse_y); }
	void idle_func(void){ main_obj->glut_idle_func(); }
}

int main(int argc, char **argv){
    std::vector<Slide *> slides; 
	global_options = new Options;
	
	argc--; argv++;
	if( argc <= 0 or !strcmp(*argv,"-") ){
		// Read options and images from the standart input
		// The format of the input is specified in the 
		Parser parser(std::cin);
    	parser( global_options, &slides);
		if(slides.size() == 0)
			return 0;
	}else{
		while(argc > 0){
			Slide * slide = new Slide;
			slide->image = new ImageData(*argv);
			slides.push_back(slide);
			argc--; argv++;
		}

	}

	// GLUT initialization - window creation etc.
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(640,480);
	glutCreateWindow("Obr");


	// Two job queues
	TaskMutexQueue job_queue; 		// give jobs to the thread loading data from the disk
	TaskMutexQueue result_queue;	// the thread gives the results back

	// Run data loading thread
	ThreadWorker wrk(job_queue, result_queue);
	boost::thread thrd(wrk);

  	// Create the main drawing object
	global_glut::main_obj = new Main(slides, job_queue, result_queue);

	// Init glut functions
	glutSpecialFunc(global_glut::special_keyboard_event);
	glutKeyboardFunc(global_glut::normal_keyboard_event);
	glutDisplayFunc(global_glut::render_scene);
	glutIdleFunc(global_glut::idle_func);
	glutReshapeFunc(global_glut::reshape_window);

	// Enter the main loop and never return
	glutMainLoop();
	
	// This actually never happens :-)
	thrd.join();
	return(0);
}
