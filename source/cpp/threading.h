#ifndef _THREADINGH
#define _THREADINGH

//#include <assert.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/xtime.hpp>
#include <vector>
#include <queue>
#include <utility>
#include <stdexcept>
/*		State enum used in StateObject		*/
namespace State{
enum State{
    Error=-1, Empty=0, Ready=1, Used=2, ImlibReady=3, Hell=666
};
}
/*		Signal enum used in Task to			*/
/*		signalize what type of data			*/
/*		is present, or to pass a basic		*/
/*		messaging.							*/
namespace Signal{
enum Signal{
	OK=0, Quit=1, SigBadAlloc=2, Error=3
};
}

/*		Class with mutexed state and		*/
/*		basic predicates to test it.		*/
class StateObject{
public:
	int state;

	bool test_ready(){
		boost::mutex::scoped_lock state_lock(state_mutex);
		return state == State::Ready;
	}
	bool test_imlib_ready(){
		boost::mutex::scoped_lock state_lock(state_mutex);
		return state == State::ImlibReady;
	}
	bool test_empty(){
		boost::mutex::scoped_lock state_lock(state_mutex);
		return state == State::Empty;
	}
	bool test_error(){
		boost::mutex::scoped_lock state_lock(state_mutex);
		return state == State::Error;
	}

	boost::mutex state_mutex;
	StateObject():state(State::Empty) {}
};

/*		Default task class, to be used witin		*/
/*		the TaskMutexQueue							*/
class Task{
public:
	Task() : sig(Signal::OK) {}
	Task(int _sig) : sig(_sig) {}
    virtual Task * do_it(){ return 0; };
	virtual int signal(){ return sig;}
protected:
	int sig;
};


/*		The TaskMutexQueue template, usable			*/
/*		In the STL manner.							*/
template <typename Key, typename Data, class Compare>
class priority_mutex_queue 
{
public:
        typedef Key                     key_type;
        typedef Data                    data_type;
        typedef Compare                 compare_type;
        typedef std::pair<Key, Data>    value_type;
		/*		Get the Data with the biggest		*/
		/*		priority							*/
        data_type get(){
                value_type p = get_pair();
                return p.second;
        }
		/*		Push data with some priority		*/
        void push(data_type q, key_type key){
				push(std::make_pair(key, q));
        }
		/*		Push data with some 0 priority		*/
        void push(data_type q){
				push(q, 0);
        }
		/*		Push pair (data, priority) in.		*/
        void push(value_type pair){
                boost::mutex::scoped_lock queue_lock(mutex);
                pq.push(pair);
        }
		/*		Get pair (data, priority) out.		*/
        value_type get_pair(){
                boost::mutex::scoped_lock queue_lock(mutex);
                value_type p = pq.top();
                pq.pop();
                return p;
        }
		/*		Predicate to test if the queue		*/
		/*		is empty							*/
        bool empty(){
                boost::mutex::scoped_lock queue_lock(mutex);
                return pq.empty();
        }

        class compare: public std::binary_function<value_type, value_type, bool> {
        public:
                bool operator()(const value_type& x, const value_type& y) const
                { return compare_type()(x.first, y.first); }
        };

private:
        std::priority_queue<value_type, std::vector<value_type>, compare> pq;

        boost::mutex mutex;
};

/*		Typedef used throughout the program			*/
/*		priority queue with Task and int priority	*/
typedef priority_mutex_queue<int, Task *, std::less<int> > TaskMutexQueue;
// the bigger the priority => execution first

/*		Class for thread, working on Tasks it		*/
/*		loads from the queue.						*/
class ThreadWorker
{
public:
    ThreadWorker(TaskMutexQueue & tmq_in, TaskMutexQueue & tmq_out) : in_job_queue(tmq_in), out_job_queue(tmq_out), job_num(0) {}
	/*		The main loop (function called			*/
	/*		by boost::thread						*/
    void operator()();
private:
	void sleep(void);
	/*		Queue with jobs				*/
	TaskMutexQueue & in_job_queue;
	/*		Queue to push results		*/
	TaskMutexQueue & out_job_queue;
	unsigned job_num;
};

#endif
