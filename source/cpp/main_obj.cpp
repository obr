#include "main_obj.h"

extern Options * global_options;
extern boost::mutex global_iomutex;
using namespace std;

// Main Object constructor
Main::Main( std::vector<Slide *> & slds, TaskMutexQueue & tmq, TaskMutexQueue & res_tmq ):
				rotation(0.0),
				 slideshow(0),current_slide(0),move_direction(1),
				slides(slds), job_queue(tmq), result_queue(res_tmq), job_manager(tmq, res_tmq),
				imf_left(0), imf_right(0),show_info(3),
				show_state(Const::ImageView), screen_width(0), screen_height(0)
				{
	// if nothing to show
	if(slides.begin() == slides.end())
		my_exit(); // end
	
	// order thumbnails (if present) with low priority
	int prio = 0;
	for(std::vector<Slide *>::iterator it = slides.begin() ; it != slides.end() ; ++it){
		job_manager.order_image((*it)->thumbnail, prio);
		prio--;
	}
	// order first image and images around it
	image_view_order_images();
	// init objects
	ft_font = new FTGLPixmapFont(global_options->get_font().c_str());	// font drawing object
	imf = new ImageFace( *(slds.begin()), ft_font);				// front image face
	tmf = new ThumbnailFace( slides, job_manager);				// face showing thumbnails
	inff = new InfoFace(ft_font);								// face showing text information like slideshow status
	// update text shown
	inff->update(current_slide, slides.size(), slideshow);

	// init aging time
	boost::xtime_get(&xt_last_aging, boost::TIME_UTC);

	glut_reshape_window(640, 480);

	// reset timers
	time_all=0; time_draw=0;time_gl=0;time_aging=0;
}

// function called at exit, kills the working thread, print info about images and stats
void Main::my_exit(){
	job_manager.quit();
	sleep(50);

	// print & delete images
	for(std::vector<Slide *>::iterator it = slides.begin() ; it != slides.end() ; ++it){
		(*it)->print();
		delete (*it)->image;
		delete (*it)->histogram;
		delete (*it)->thumbnail;
	}
	// print stats
	if(global_options->get_verbose()) {
		boost::mutex::scoped_lock io_lock(global_iomutex);
		cerr << endl;
		cerr << "idle total time: " << time_all <<" msec"<< endl;
		cerr << "draw total time: " << time_draw <<" msec" << endl;
		cerr << "   gl load time: " << time_gl <<" msec" << endl;
		cerr << "aging load time: " << time_aging <<" msec" << endl;
	}
	// die
	exit(0);
}

// this function is called whenever glut has nothing to do
// in such situation, we draw, move and load textures
void Main::glut_idle_func(void){
	boost::xtime xt_start; boost::xtime_get(&xt_start, boost::TIME_UTC);
	
	// if we are in a transition state, move
	if(show_state == Const::SmoothTransitionLeft || show_state == Const::SmoothTransitionRight ||
		show_state == Const::SmoothTransitionToThumbnailView || show_state == Const::SmoothTransitionFromThumbnailView)
		do_transition();
	if(slideshow)
		do_slideshow();

	boost::xtime xt_start_ren; boost::xtime_get(&xt_start_ren, boost::TIME_UTC);
	// render scene
	glut_render_scene();
	boost::xtime xt_end_ren; boost::xtime_get(&xt_end_ren, boost::TIME_UTC);
	time_draw += 1000*(xt_end_ren.sec - xt_start_ren.sec ) + (xt_end_ren.nsec - xt_start_ren.nsec )/1000000;

	// if there is a new image loaded from disk, we load its texture
	if(!result_queue.empty()){
		boost::xtime xt_start_gl; boost::xtime_get(&xt_start_gl, boost::TIME_UTC);
		load_gl_data();
		boost::xtime xt_end_gl; boost::xtime_get(&xt_end_gl, boost::TIME_UTC);
		time_gl += 1000*(xt_end_gl.sec - xt_start_gl.sec ) + (xt_end_gl.nsec - xt_start_gl.nsec )/1000000;
	}
	// if we have nothing to do
	if(result_queue.empty()){
		boost::xtime xt_start_aging; boost::xtime_get(&xt_start_aging, boost::TIME_UTC);
		// age image data
		do_aging();
		boost::xtime xt_end_aging; boost::xtime_get(&xt_end_aging, boost::TIME_UTC);
		time_aging += 1000*(xt_end_aging.sec - xt_start_aging.sec ) + (xt_end_aging.nsec - xt_start_aging.nsec )/1000000;
	}
	
	{
		boost::xtime xt_end;
		boost::xtime_get(&xt_end, boost::TIME_UTC);

		time_all += 1000*(xt_end.sec -xt_start.sec ) + (xt_end.nsec - xt_start.nsec )/1000000;
	}
	// if we have nothing to do, sleep (to decrease the CPU usage)
	if(result_queue.empty()){
		if(show_state == Const::SmoothTransitionLeft || show_state == Const::SmoothTransitionRight ||
			show_state == Const::SmoothTransitionToThumbnailView || show_state == Const::SmoothTransitionFromThumbnailView)
			sleep(10);
		else
			sleep(60);
	}
}
// Ages image data for them not to be loaded when not needed
// this applies to GL textures as well as imlib data
void Main::do_aging(){
	boost::xtime xt_now;
	boost::xtime_get(&xt_now, boost::TIME_UTC);
	unsigned long ms = 1000*(xt_now.sec - xt_last_aging.sec ) + (xt_now.nsec - xt_last_aging.nsec )/1000000;
	// do the aging every half of second
	if(ms > 500){
		xt_last_aging = xt_now;
		// for each slide
		bool change  =0;
		for(unsigned i = 0 ; i < slides.size() ; i++ ){
			if( (int) i < proximity_left() || (int) i > proximity_right() ){
				change |= age_image(slides[i]->image, 5);			// image data - supposedly the biggest - ages fast
				change |= age_image(slides[i]->histogram, 5);
				change |= age_image(slides[i]->thumbnail, 60);	// thumbnails age slower
			}
		}
		if(change){
			imf->reshape(screen_width,screen_height);
			tmf->reshape(screen_width,screen_height);
		}
	}
}
// Age images, we keep imlib data (loaded from disk) 3 times longer than GL data (loaded fast from imlib data)
bool Main::age_image(ImageData * image, unsigned at_age ){
	bool change  =0;
	if(image){
		if(image->test_ready()){
			image->age++;
			if(image->age >= at_age ){
				image->delete_data();
				change = 1;
			}
		}else
		if(image->test_imlib_ready()){
			image->age++;
			if(image->age >= 3 * at_age)
				image->delete_imlib_data();
		}
	}
	return change;
}
// If we have run out of memory, delete some data
// This should not happen very often; aging prevents this
void Main::force_remove_old(){
	if(global_options->get_verbose()) {
	boost::mutex::scoped_lock io_lock(global_iomutex);
	cerr << "Force remove." << endl;
	}
	unsigned oldest_age=0;
	ImageData * oldest_image=(*slides.begin())->image;
	// for each slide, check if it is the oldest
	for(std::vector<Slide *>::iterator it = slides.begin() ; it != slides.end() ; ++it){
		check_old((*it)->image, oldest_age, oldest_image);
		check_old((*it)->histogram, oldest_age, oldest_image);
		check_old((*it)->thumbnail, oldest_age, oldest_image);
	}
	// remove all oldest slides (their data)
	for(std::vector<Slide *>::iterator it = slides.begin() ; it != slides.end() ; ++it){
		if((*it)->image->age >= oldest_age)
			(*it)->image->delete_all();
	}
	imf->reshape(screen_width,screen_height);
	tmf->reshape(screen_width,screen_height);
}

void Main::check_old(ImageData * image, unsigned & age_old, ImageData * & image_old){
		if(image && (image->test_ready() || image->test_imlib_ready()) && image->age > age_old){
			age_old=image->age;
			image_old=image;
		}
}

int Main::proximity_left(){
	return 	move_direction > 0 ? current_slide - 1 : current_slide - 2;
}
int Main::proximity_right(){
	return 	move_direction > 0 ? current_slide + 2 : current_slide + 1;
}

// Orders images that are around the current one
// images are loaded such that closer the image is to the current one, the sooner it gets loaded
void Main::image_view_order_images(){
	int	first =  proximity_left();
	int	last =  proximity_right();
	int two_times_current = 2 * current_slide;

	if( last - current_slide >= current_slide - first){
		for(int i = last ; two_times_current - first < i ; i--)
			load_checked(i);
		for(int i = current_slide - first ; i > 0 ; i -- ){
			load_checked(current_slide + i);
			load_checked(current_slide - i);
		}
		load_checked(current_slide);
	}else{
		for(int i = first ; two_times_current- last > i ; i++)
			load_checked(i);
		for(int i = last - current_slide ; i > 0 ; i -- ){
			load_checked(current_slide + i);
			load_checked(current_slide - i);
		}
		load_checked(current_slide);
	}
}

// Encapsulates ordering so that only valid images are shown
bool Main::load_checked(int index, int priority){
	if( index >=0 && index < (int) slides.size()){
		return	job_manager.order_image(slides[index]->histogram, priority) |\
				job_manager.order_image(slides[index]->image, priority) | \
				job_manager.order_image(slides[index]->thumbnail, priority);
	}
	return 0;
}
bool Main::load_checked(int index){
	if( index >=0 && index < (int) slides.size())
		return	job_manager.order_image(slides[index]->histogram) |\
				job_manager.order_image(slides[index]->image) | \
				job_manager.order_image(slides[index]->thumbnail);
	return 0;
}

// Loads opengl textures from data present in result_queue
// if we run out of memory, or we are informed about it (through the queue) 
// we force some memory free using force_remove_old()
void Main::load_gl_data(){
	int i = 1;
	bool change =0;
	while(!result_queue.empty() && i > 0){
		i--;
		// get the container from the queue
		TaskImageContainer * container = (TaskImageContainer *) result_queue.get();
		if( container->signal() == Signal::OK ){
			if( container->data->test_imlib_ready() ){
				try{
					// Load opengl texture !!
					container->data->load_data();
					if(global_options->get_verbose()) {
						boost::mutex::scoped_lock io_lock(global_iomutex);
						cerr << "Loaded GL: " << container->data->name << endl;
					}
					change =1;
				}
				// if we run out of memory
				catch(std::bad_alloc){
					// delete some old textures and data
					force_remove_old();	
					// retry
					job_manager.order_image(container->data);
				}
				catch(std::exception){
					boost::mutex::scoped_lock io_lock(global_iomutex);
					cerr << "A GL error occured. Exiting." << endl;
					delete container;	
					my_exit();	
				}
			}
		}else
		// if we run out of memory
		if( container->signal() == Signal::SigBadAlloc){
			// delete some old textures and data
			force_remove_old();	
			// retry
			job_manager.order_image(container->data);
		}

		delete container;	
	}
	if(change){
		imf->reshape(screen_width,screen_height);
		tmf->reshape(screen_width,screen_height);
	}
}

// Function called by glut when user reshapes window
void Main::glut_reshape_window(int new_width, int new_height){
	screen_width=new_width; screen_height=new_height;

	// compute new camera position	
	GLfloat h = (GLfloat) screen_height / (GLfloat) (screen_width << 1) ;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glFrustum(-0.5, 0.5, -h, h, 2.0, 90.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, (GLint) screen_width, (GLint) screen_height);

	// Reshape textures ( sets coordinates accordng to the current screen size )
	imf->reshape(screen_width,screen_height);
	tmf->reshape(screen_width,screen_height);
	inff->reshape(screen_width,screen_height);
}

// Function called by glut whenever window redraw is needed
// we call it when idle to increase FPS and thus make user experience more pleasant :-)
void Main::glut_render_scene(void){
	// reset color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// reset object position
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -5.01);
	
	// draw objects according to current state
	switch(show_state){
		case Const::ThumbnailView:
			// draw thumbnails
			glPushMatrix();
			tmf->draw(0);
			glPopMatrix();
			break;

		case Const::ImageView:
			// draw front image face
			glPushMatrix();
			imf->draw(show_info);
			glPopMatrix();
			// draw info face
			glPushMatrix();
			inff->draw(show_info);
			glPopMatrix();
			break;

		case Const::SmoothTransitionLeft: 
			// rotate
			glRotatef(rotation, 0.0, 1.0, 0.0);
			// draw front image face
			glPushMatrix();
				imf->draw(0);
			glPopMatrix();
			// draw left image face
			glPushMatrix();
				glRotatef(-90, 0.0, 1.0, 0.0);
				imf_left->draw(0);
			glPopMatrix();
			break;
		case Const::SmoothTransitionRight:
			// rotate
			glRotatef(rotation, 0.0, 1.0, 0.0);
			// draw front image face
			glPushMatrix();
				imf->draw(0);
			glPopMatrix();
			// draw right image face
			glPushMatrix();
				glRotatef(90, 0.0, 1.0, 0.0);
				imf_right->draw(0);
			glPopMatrix();
			break;
		case Const::SmoothTransitionToThumbnailView:
		case Const::SmoothTransitionFromThumbnailView:
			
			// draw front image face
			glRotatef(rotation, 1.0, 0.0, 0.0);
			glPushMatrix();
				imf->draw(0);
			glPopMatrix();
			// draw thumbnail image face
			glPushMatrix();
				glRotatef(-90, 1.0, 0.0, 0.0);
				tmf->draw(2);
			glPopMatrix();
			break;
		default: break;
	}

	// do double buffering swap
    glFlush();
	glutSwapBuffers();
}

// This function handles keys
void Main::keyboard_event(Const::Keys::KeyReturn ret){
	using namespace Const::Keys;

	// escape => exit
	if(ret.keycode == Escape){
		my_exit();
	}else
	if(show_state == Const::ThumbnailView){
		switch(ret.keycode){
			case Escape: my_exit();			break;
			case Left:	tmf->move_left();	break;
			case Right:	tmf->move_right();	break;
			case Up:	tmf->move_up(); 	break;
			case Down:	tmf->move_down();	break;
			case Home:	tmf->move_home();	break;
			case End:	tmf->move_end();	break;
			case Thumbnails:
			case Space: transition_from_thumbnails(); break;
			default: break;
		}
	}else
	if(show_state == Const::ImageView){
		switch(ret.keycode){
			case Up:	rotate_current(270);		break;
			case Down:	rotate_current(90);			break;
			case Left:  transition_left();			break;
			case Right: transition_right();			break;
			case Info:	toggle_info();				break;
			case Mark:	toggle_marked_current();	break;
			case Slideshow: toggle_slideshow();		break;
			case Space:
			case Thumbnails: transition_to_thumbnails(); break;
			default: break;
		}
	}else
	// keys pressed during transition (e.g. right arrow stops transition to left
	if(show_state == Const::SmoothTransitionLeft && ret.keycode == Right)
		transition_right();
	else
	if(show_state == Const::SmoothTransitionRight && ret.keycode == Left)
		transition_left();
}

// recomputes begining of transition if it has been reversed
void Main::reverse_time_in_transition(){
	boost::xtime xt_now;
	boost::xtime_get(&xt_now, boost::TIME_UTC);
	
	unsigned ms = 1000*(xt_now.sec - xt_rotation_start.sec) + ((long) (xt_now.nsec - xt_rotation_start.nsec)/1000000);
	
	int rest = global_options->get_rotation_delay() - ms;
		
	xt_rotation_start.sec = xt_now.sec - rest / 1000;
	xt_rotation_start.nsec = xt_now.nsec - (rest %1000)*1000000;
}

// Move left
void Main::transition_left(){
	slideshow=0;
	// if we can  and we are in normal view
	if(current_slide > 0 && show_state == Const::ImageView){
		// update current_slide
		move_prev();
		// if smooth transition, init it (load left image face, update start time)
		if(global_options->get_transition_effect() == Const::TransitionSmooth){
			show_state = Const::SmoothTransitionLeft;
			boost::xtime_get(&xt_rotation_start, boost::TIME_UTC);

			if(imf_left)
				throw std::runtime_error("Unfreed imf_left");
			imf_left = new ImageFace( slides[current_slide], ft_font);
			imf_left->reshape(screen_width, screen_height);
		}else
		if(global_options->get_transition_effect() == Const::TransitionSkip){
			delete imf;	
			imf = new ImageFace( slides[current_slide], ft_font);
			imf->reshape(screen_width, screen_height);
		}
	// if we are canceling right transition
	}else if( show_state == Const::SmoothTransitionRight ){
		show_state = Const::SmoothTransitionLeft;
		move_prev();

		if(imf_left)
			throw std::runtime_error("Unfreed imf_left");
		imf_left = imf;
		imf = imf_right;
		imf_right=0;
		
		reverse_time_in_transition();
	}
}

// Move left
void Main::transition_right(){
	// if we can  and we are in normal view
	if(current_slide < slides.size()-1 && show_state == Const::ImageView){
		// update current_slide
		move_next();
		
		// if smooth transition, init it (load right image face, update start time)
		if(global_options->get_transition_effect() == Const::TransitionSmooth){
			show_state = Const::SmoothTransitionRight;
			boost::xtime_get(&xt_rotation_start, boost::TIME_UTC);

			if(imf_right)
				throw std::runtime_error("Unfreed imf_right");
			imf_right = new ImageFace( slides[current_slide], ft_font);
			imf_right->reshape(screen_width, screen_height);
		}else
		if(global_options->get_transition_effect() == Const::TransitionSkip){
			delete imf;	
			imf = new ImageFace( slides[current_slide], ft_font);
			imf->reshape(screen_width, screen_height);
		}
	// if we are canceling left transition
	}else if( show_state == Const::SmoothTransitionLeft ){
		show_state = Const::SmoothTransitionRight;
		move_next();

		if(imf_right)
			throw std::runtime_error("Unfreed imf_right");
		imf_right = imf;
		imf = imf_left;
		imf_left=0;

		reverse_time_in_transition();
	}
}
// Two functions to update current slide, move in thumbnail face and update information
void Main::move_next(){
	if(current_slide < slides.size()-1 ){
		current_slide++;
		tmf->move_next();
		move_direction=1;
		inff->update(current_slide, slides.size(), slideshow);
		inff->reshape(screen_width,screen_height);
		image_view_order_images();
	}
}
void Main::move_prev(){
	if(current_slide > 0 ){
		current_slide--;
		tmf->move_prev();
		move_direction=-1;
		inff->update(current_slide, slides.size(), slideshow);
		inff->reshape(screen_width,screen_height);
		image_view_order_images();
	}
}

// initialize transition to thumbnails
void Main::transition_to_thumbnails(){
	if(slideshow)
		toggle_slideshow();
	if(show_state == Const::ImageView){
		if(global_options->get_transition_effect() == Const::TransitionSmooth){
			show_state = Const::SmoothTransitionToThumbnailView;
			boost::xtime_get(&xt_rotation_start, boost::TIME_UTC);
		}else
			show_state = Const::ThumbnailView;
		// order thumbnails that will be shown
		tmf->order_necessary_images();
	}
}
// initialize transition from thumbnails
void Main::transition_from_thumbnails(){
	if(show_state == Const::ThumbnailView){
		if(global_options->get_transition_effect() == Const::TransitionSmooth){
			show_state = Const::SmoothTransitionFromThumbnailView;
			boost::xtime_get(&xt_rotation_start, boost::TIME_UTC);
		}else
			show_state = Const::ImageView;
		if(current_slide != tmf->get_current()){
			current_slide = tmf->get_current();
			delete imf;
			imf = new ImageFace(slides[current_slide], ft_font);
			imf->reshape(screen_width, screen_height);
			inff->update(current_slide, slides.size(), slideshow);
			inff->reshape(screen_width,screen_height);
		}
		// order thumbnails that will be shown
		image_view_order_images();
	}
}
// modify rotation in a transition according to time that has passed from the transition beginning
void Main::do_transition(){
	boost::xtime xt_now;
	boost::xtime_get(&xt_now, boost::TIME_UTC);
	// get time from the beginning
	unsigned ms = 1000*(xt_now.sec - xt_rotation_start.sec) + ((long) (xt_now.nsec - xt_rotation_start.nsec)/1000000);
	// if transition has finished
	if( ms >= global_options->get_rotation_delay()){
		switch(show_state){
			case Const::SmoothTransitionLeft:
				show_state = Const::ImageView;
				delete imf;	imf = imf_left; imf_left = 0;
				break;
			case Const::SmoothTransitionRight:
				show_state = Const::ImageView;
				delete imf;	imf = imf_right; imf_right = 0;
				break;
			case Const::SmoothTransitionToThumbnailView:
				show_state = Const::ThumbnailView;
				break;
			case Const::SmoothTransitionFromThumbnailView:
				show_state = Const::ImageView;
				break;
			default:
				break;
		}
		rotation = 0.0;	
	// else update rotation
	}else{
		GLfloat rot_pom = (GLfloat) ((GLfloat) 90.0 *  ms) / global_options->get_rotation_delay();
		switch(show_state){
			case Const::SmoothTransitionLeft:
			case Const::SmoothTransitionToThumbnailView: rotation = rot_pom ; break;
			case Const::SmoothTransitionRight: rotation = - rot_pom; break;
			case Const::SmoothTransitionFromThumbnailView: rotation = 90.0 - rot_pom; break;
			default: break;
		}	
	}
}
// Do the slideshow if it is the time
void Main::do_slideshow(){	
	boost::xtime xt_now;
	boost::xtime_get(&xt_now, boost::TIME_UTC);
	// get the time from the beginning
	unsigned ms = 1000*(xt_now.sec - xt_slideshow_start.sec) + ((long) (xt_now.nsec - xt_slideshow_start.nsec)/1000000);
	// if it is just the time to move to next slide
	if(ms > global_options->get_slideshow_delay()){
		xt_slideshow_start = xt_now;
		unsigned pom=current_slide;
		// move there!!
		transition_right();
		if(current_slide == pom){
			slideshow = 0;
			inff->update(current_slide, slides.size(), slideshow);
			inff->reshape(screen_width,screen_height);
		}
	}
}
// sleep.... for a few ms
void Main::sleep(unsigned ms){
		boost::xtime xt;
		boost::xtime_get(&xt, boost::TIME_UTC);
		xt.nsec += ms * 1000000;
		boost::thread::sleep(xt);
}

// if user rotates an image
void Main::rotate_current(unsigned rot){
	// remember it
	slides[current_slide]->rot = (slides[current_slide]->rot + rot) %360;
	// compute new texture coordinations 
	imf->reshape(screen_width, screen_height);
	tmf->reshape(screen_width, screen_height);
}
// toggle info modes from the most verbose to the most silent
void Main::toggle_info(){
	switch(show_info){
		case 0: show_info = 1; break;
		case 1: show_info = 3; break;
		case 3: show_info = 7; break;
		case 7: show_info = 0; break;
	}
}
// mark and unmark current image
void Main::toggle_marked_current(){
	slides[current_slide]->marked = !slides[current_slide]->marked;
}
//  start the slideshow  if it is not running, stop it otherwise
void Main::toggle_slideshow(){
	slideshow = !slideshow; 
	boost::xtime_get(&xt_slideshow_start, boost::TIME_UTC);
	inff->update(current_slide, slides.size(), slideshow);
	inff->reshape(screen_width,screen_height);
}
// GLUT has two keyboard handling functions and I do not like it
void Main::glut_special_keyboard_event(int key, int, int){
	keyboard_event( global_options->translate_from_glut_special_keycode(key) );
}
void Main::glut_normal_keyboard_event(unsigned char key, int, int){
	keyboard_event( global_options->translate_from_glut_normal_keycode(key) );
}
