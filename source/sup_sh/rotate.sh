#!/bin/sh
[ -z "$1" -o -z "$2" -o ! -e "$1" ] && exit

if [ -n "$OBR_OPTIONS" ] ; then
	. "$OBR_OPTIONS"
else
OBR_OPTIONS_PLACEHOLDER
fi
image="$1"
rotation="$2"

echo "Rotate: $image" >&2
$mogrify -rotate $rotation $image
