#!/bin/sh
[ -z "$1" ] && exit
[ -e "$1" ] || exit

rot_flag=''
if [ "$(cat $1 | sed '/^Rotation/!d;/^Rotation0/d' | wc -l)" -gt 0 ] ; then
	echo -n "Do you want to rotate images? [Yes/no] " >&2
	read a </dev/tty
	rot_flag="$(echo ${a}yes | sed '/^[Nn]/d')"
fi

marked_flag=''
if [ "$(cat $1 | sed '/^Marked1/!d' | wc -l)" -gt 0 ] ; then
	echo -n "Do you want to print marked images to stdout? [Yes/no] " >&2
	read a </dev/tty
	marked_flag="$(echo ${a}yes | sed '/^[Nn]/d')"
fi

[ -z "$marked_flag" -a -z "$rot_flag" ] && exit

tmp_file="/tmp/obr_temp3.$$"
[ -w "$tmp_file" ] && rm $tmp_file

if [ -n "$OBR_OPTIONS" ] ; then
	. "$OBR_OPTIONS"
else
OBR_OPTIONS_PLACEHOLDER
fi

job_pipe="/tmp/obr_job_pipe.$$"
mkfifo $job_pipe
exec 5<>$job_pipe
$job_doer_ex $job_pipe  &

trap "rm -f $job_pipe $tmp_file ; exit 0" 0
trap "rm -f $job_pipe $tmp_file ; exit 1" 1 2 3 15

state='Start'
image=''
thumbnail=''
rotation=''
marked=''

cat $1 | while read a; do
	[ "$state" = "Start" -a "$a" = "BeginSlide" ] && {  state=SlideBlock ; continue ; }
		[ "$state" = "SlideBlock" -a "$a" = "Image" ] && {  state=ReadImage ; continue ; }
			[ "$state" = "ReadImage" ] && {  image="$a" ; state=SlideBlock ; continue ; }
		[ "$state" = "SlideBlock" -a "$a" = "Thumbnail" ] && {  state=ReadThumbnail ; continue ; }
			[ "$state" = "ReadThumbnail" ] && { thumbnail="$a" ; state=SlideBlock ; continue ; }
		[ "$state" = "SlideBlock" -a "$a" = "Marked1" ]  && { marked=True ; continue ; }
		[ "$state" = "SlideBlock" -a -n "$rot_flag" ] && {
			rot="$(echo $a | sed -n 's/^Rotation\([0-9]*\)$/\1/p')" 
			[ -n "$rot" ] && {  rotation="$rot"; continue ; }
		}
		[ "$state" = "SlideBlock" -a "$a" = "EndSlide" ] && {
			[ -n "$rot_flag" ] && [ -n "$rotation" -a "$rotation" -gt 0 ] && {
				echo "$rotate_ex $image $rotation" >> $job_pipe
				[ -n "$thumbnail" ] && echo "$rotate_ex $thumbnail $rotation" >> $job_pipe
			} 
			[ -n "$marked_flag" -a -n "$marked" ] && {
				echo $image >> $tmp_file
				[ -n "$thumbnail" ] && echo $thumbnail >> $tmp_file
			} 
			state='Start'
			image=''
			thumbnail=''
			rotation=''
			marked=''
			continue
		}
done

echo exit >> $job_pipe
while [ -p "$job_pipe" ] ; do
	sleep 0.1
done

[ -e "$tmp_file" ] && { cat $tmp_file ; rm $tmp_file ; }

