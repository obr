#!/bin/sh
[ -z "$1" -o -z "$2" ] && exit

if [ -n "$OBR_OPTIONS" ] ; then
	. "$OBR_OPTIONS"
else
OBR_OPTIONS_PLACEHOLDER
fi
image="$1"
thumbnail="$2"
histogram=''
[ -n "$3" ] && histogram="$3"

[ -e "$image" ] || exit

if [ ! -e "$thumbnail" ] ; then
	echo "Generating thumbnail: $thumbnail" >&2
	if [ -z "$(echo $image | sed '/\.jp[e]\{,1\}g$/d')" ]; then
		$convert -define jpeg:size=512x512  $image -thumbnail 256x256 -unsharp 0x.5 $thumbnail
	else
		$convert $image -thumbnail 256x256 -unsharp 0x.5 $thumbnail
	fi
fi

if [ -n "$histogram" -a -e "$thumbnail" -a ! -e "$histogram" ] ; then
	echo "Generating histogram: $histogram" >&2
	tmp_file="/tmp/obr_tmp.$$.jpg"
	$convert $thumbnail -resize 50% $tmp_file
	$histog_ex -c 2 -l 'Red Green Blue' $tmp_file $histogram
	rm -f $tmp_file
fi
