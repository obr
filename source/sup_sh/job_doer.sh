#!/bin/sh
[ -z "$1" ] && exit
job_pipe=$1
[ -p "$job_pipe" ] || exit

done_flag="/tmp/obr_done.$$_"
jobs_max=2
jobs_running=0
job=0

trap "rm -f $job_pipe ; rm -f ${done_flag}* 2>/dev/null ; exit 0" 0
trap "rm -f $job_pipe ; rm -f ${done_flag}* 2>/dev/null ; exit 1" 1 2 3 15

 while read cmd ; do
	[ "$cmd" = 'exit' ] && { jobs_max=1 ; }

	while [ "$jobs_running" -ge "$jobs_max" ] ; do
		if [ "$(ls ${done_flag}* 2>/dev/null | wc -l)" -gt 0 ] ; then
			for fl in ${done_flag}* ; do
				rm $fl
				jobs_running=$(($jobs_running - 1))
			done
		else
			sleep 0.1
		fi
	done

	if [ ! -p $job_pipe -o "$cmd" = 'exit' ] ; then
		exit 0
	fi

	{ $cmd ; touch "${done_flag}${job}" ; } &
	jobs_running=$(($jobs_running + 1))
	job=$(($job + 1))
done < "$job_pipe"
