#!/bin/sh
[ "$(whoami)" = 'root' ] && { echo "Please use this installator for local per-user installation only!" ; exit 0 ; }

[ "$1" = '-h' -o "$1" = '--help' ] && {
cat <<EOF
Usage: $0 [OPTIONS]

OPTIONS
  -h, --help         shows this useful help.
  -d DIR             installs to DIR instead of asking user.
EOF
exit
}

default_obr_dir="$HOME/.obr"
obr_version="0.1"
source_dir='source'
verbose=''
obr_dir=''
while [ "$#" -gt '0' ] ; do 
	[ "$1" = '-v' -o "$1" = '--verbose' ] && { verbose='true' ; shift 1 ; continue ; }
	[ "$1" = '-d' ] && {
		[ -n "$2" ] && obr_dir="$2" || { echo "Error: no directory provided." >&2 ; exit 1 ; }
		shift 2
		continue
	}
	echo "Error: Unrecognized option '$1'." >&2
	exit 1
done 

echo "Hello! This is a simple installator for Obr image viewer and Obr photo album manager."
echo
if [ -z "$obr_dir" ] ; then
	echo -n "Please enter the installation directory: [$default_obr_dir] "
	read obr_dir
fi

[ -z "$obr_dir" ] && obr_dir="$default_obr_dir"
bin_dir="$obr_dir/bin"
album_sh="$obr_dir/album_sh"
sup_sh="$obr_dir/sup_sh"
database="$obr_dir/obr.db"

echo -n "Creating directories..."
[ -e "$obr_dir" ] && { rm -rf "$obr_dir" "$bin_dir" "$album_sh" "$sup_sh" ; }
mkdir "$obr_dir" 
[ ! -d "$obr_dir" ] && { echo "Error: Directory '$obr_dir' cannot be created. Exit." >&2 ; exit 1 ; }
[ ! -w "$obr_dir" ] && { echo "Error: Directory '$obr_dir' is not writable. Exit." >&2 ; exit 1 ; }
mkdir "$bin_dir" || exit
mkdir "$album_sh" || exit
mkdir "$sup_sh" || exit
echo " done"

echo -n "Making the source..."
[ -e "$source_dir" ] || { echo "Error: Directory '$source_dir' does not exist. Exit." >&2 ; exit 1 ; }
( cd $source_dir/cpp ; make clean ;  make ) || exit
echo " done"

echo "Obr $obr_version" > "${obr_dir}/version"

cat <<EOF > "${bin_dir}/obr_album"
#!/bin/sh

bash="\$(which bash)"
[ -z "\$bash" ] && { echo "Error: could not find bash. Exit." ; exit 1 ; }
bash --rcfile ${obr_dir}/rcfile
EOF
chmod +x "${bin_dir}/obr_album"
touch "${obr_dir}/histfile"

cat <<EOF > "${obr_dir}/rcfile"
export PS1="obr album> "
export OBR_HOME="$obr_dir"
export OBR_OPTIONS="\$OBR_HOME/options.sh"
export OBR_OLD_PATH="\$PATH"
export OBR_DATABASE="\$OBR_HOME/obr.db"
export HISTFILE="\$OBR_HOME/histfile"
export LAST="/tmp/obr_last_result.\$$"

rm="\$(which rm)"
trap "\$rm -f \$LAST ; exit 0 ; " 0
trap "\$rm -f \$LAST ; exit 1 ; " 1 2 3 15

cat="\$(which cat)"
cat "\${OBR_HOME}/version"
cat <<MEND
Welcome to Obr album. To see the help type 'help'

MEND
function help(){
cmds='add_to_collection list_photos show delete list_albums new_photo new_album delete_photo delete_album is_photo is_album'

function help_fc(){
\$cat <<EOF2
What do you want to help with??
e.g
	obr album> help add_to_collection

There are following commands available:
EOF2
for a in \$cmds ; do
	echo -n "\${a}: "
	eval "\$a --short-help"
done
echo 
}

if [ -z "\$1" ] ; then
	help_fc 
else
	helped=''
	for a in \$cmds ; do
		if [ "\$a" = "\$1" ] ; then
			eval "\$a --help"
			helped='true'
		fi
	done

	if [ -z "\$helped" ] ; then
		echo "Unknown command \$1"	
		help_fc
	fi
fi
}
export PATH="$album_sh"
EOF

cat <<EOF > "${obr_dir}/options.sh"
obr_dir="$obr_dir"
album_sh="\${obr_dir}/album_sh"
sup_sh="\${obr_dir}/sup_sh"

if [ -z "\$OBR_DATABASE" ] ; then
	database="\${obr_dir}/obr.db"
else
	database="\$OBR_DATABASE" 
fi
options="\${obr_dir}/options"

$(
for a in sqlite identify convert mogrify sed ; do
	echo "${a}=\"\$(which ${a} || echo '$(which $a)')\""
	echo "[ -z \"\$${a}\" ] && { echo \"Command '$a' not found. Exit.\">&2 ; exit 1 ; }"
done
)

obr_ex="\${sup_sh}/obr_bin"
[ ! -e "\${sup_sh}/obr_bin" ] && { echo "File '\${sup_sh}/obr_bin' not found. Exit.">&2 ; exit 1 ; }
$(
for a in thumb_hist postprocess job_doer histog rotate ; do
	echo "${a}_ex=\"\${sup_sh}/${a}.sh\""
	echo "[ ! -e \"\${sup_sh}/${a}.sh\" ] && { echo \"File '\${sup_sh}/${a}.sh' not found. Exit.\">&2 ; exit 1 ; }"
done
)

$(
for a in new_photo new_album is_album is_photo delete_album delete_photo list_albums list_photos show delete add_to_collection ; do
	echo "${a}=\"\${album_sh}/${a}\""
	echo "[ ! -e \"\${album_sh}/${a}\" ] && { echo \"File '\${album_sh}/${a}' not found. Exit.\">&2 ; exit 1 ; }"
done
)
EOF
cp "$source_dir/cpp/obr_bin" "$sup_sh" || { echo "Could not copy file '$source_dir/cpp/obr_bin'. Exit." >&2 ; exit 1 ; }
cp "$source_dir/obr" "$bin_dir" || { echo "Could not copy file '$source_dir/obr'. Exit." >&2 ; exit 1 ; }
cp "$source_dir/options" "$obr_dir" || { echo "Could not copy file '$source_dir/options'. Exit." >&2 ; exit 1 ; }
sed -i "s/OBR_OPTIONS_PLACEHOLDER/. $(echo ${obr_dir}/options.sh | sed 's/\//\\\//g')/" "$bin_dir/obr"

for a in $source_dir/album_sh/* ; do
	afile="$(echo $a | sed 's/^.*\///')"
	cp "$a" "$album_sh/$afile"
	chmod +x "$album_sh/$afile"
done
for a in $source_dir/sup_sh/* ; do
	cp "$a" "$sup_sh"
	chmod +x "$a"
done
for a in $album_sh/* $sup_sh/* ; do
	afile="$(echo $a | sed 's/^.*\///')"
	[ "$afile" = '.' -o "$afile" = '..' ] && continue
	sed -i "s/OBR_OPTIONS_PLACEHOLDER/. $(echo ${obr_dir}/options.sh | sed 's/\//\\\//g')/" "$a"
done

[ -e "$database" ] && { rm -f "$database" || exit ; }
cat <<EOF | sqlite "$database"
CREATE TABLE photo_album (photo VARCHAR(128) not null, album varchar(48) not null);
CREATE TABLE albums (album VARCHAR(48) NOT NULL PRIMARY KEY, place VARCHAR(48), description varchar(256));
CREATE TABLE photos (photo varchar(128) not null primary key, thumbnail varchar(128), histogram varchar(128), description varchar(256));
EOF


echo "Installation has finished!!"

echo "To be able to run the Obr image viewer and Obr album, your PATH needs to be modified."
echo -n "Do you want the installator to modify your .bashrc for you? [Yes/no]: "
read yes_no

if [ -z "$(echo ${yes_no} | sed '/^[Nn]/!d' )" ] ; then
	# said Yes
	[ -w "$HOME/.bashrc" ] || { echo "File $HOME/.bashrc is not writable. Exit" ; exit 1 ; }
	if [ "$(cat $HOME/.bashrc | grep 'PATH=' | grep $bin_dir | wc -l)" -gt 0 ] ; then
		echo "Directory $bin_dir already present in the PATH."
	else
		echo "Adding $bin_dir to the PATH in $HOME/.bashrc"
		sed -i "/PATH=/{s/PATH=/&$(echo $bin_dir | sed 's/\//\\\//g'):/;:a;n;ba}" "$HOME/.bashrc"
	fi
else
	# said No
	echo
	echo "OK, you should now add the following directory to your path manually:"
	echo "$bin_dir"
fi

